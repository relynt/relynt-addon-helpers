<?php

namespace relynt\components\log\targets;

use relynt\components\log\models\LogProperty;
use yii\helpers\ArrayHelper;
use yii\log\DbTarget;

/**
 * Class RelyntDbTarget
 *
 * For use this target add this code to web.php/console.php
 *
 * 'components' => [
 *      'log' => [
 *          'targets' => [
 *              [
 *                  'class' => 'relynt\components\log\targets\RelyntDbTarget',
 *                  'logTable' => 'log',
 *                  'properties' => [
 *                      'testProperty' => [
 *                          'type' => LogProperty::TYPE_STRING
 *                       ],
 *                  ]
 *              ],
 *          ]
 *      ]
 * ],
 *
 * @package relynt\components\log\targets
 */
class RelyntDbTarget extends DbTarget
{
    public $prettyStatus = false;
    public $enableActions = false;
    public $timeToStoreLogs = 60 * 60 * 24 * 30 * 6; // 6 month

    /** @var array|LogProperty */
    public $properties = [];

    private $_tableColumns;
    private $_tableValues;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->properties = new LogProperty([
            'property' => $this->properties,
        ]);
    }

    /**
     * Method for export records to database
     * @throws \yii\db\Exception
     */
    public function export()
    {
        if ($this->db->getTransaction()) {
            $this->db = clone $this->db;
        }

        $tableName = $this->db->quoteTableName($this->logTable);

        $this->generateValuesAndColumnsList();

        $sql = "INSERT INTO $tableName ($this->_tableColumns)
                VALUES ($this->_tableValues)";

        $command = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            if (!isset($message['message'])) {
                continue;
            }

            $bindValues = [
                ":message" => $message['message'],
                ":status" => $message['status'],
            ];

            foreach ($this->properties->getPropertyNames() as $column) {
                $bindValues[":$column"] = $this->getColumnValue($message, $column);
            }

            $command->bindValues($bindValues)->execute();
        }
    }

    /**
     * Get list all columns
     * @return array list all columns
     */
    public function getColumns()
    {
        return ArrayHelper::merge([
            'message',
            'status',
        ], $this->properties->getPropertyNames());
    }

    /**
     * Generate templates for request by column list
     */
    public function generateValuesAndColumnsList()
    {
        $this->_tableColumns = '';
        $this->_tableValues = '';
        foreach ($this->getColumns() as $column) {
            if (empty($this->_tableColumns) or empty($this->_tableValues)) {
                $this->_tableColumns = "[[$column]]";
                $this->_tableValues = ":$column";
                continue;
            }

            $this->_tableColumns .= ", [[$column]]";
            $this->_tableValues .= ", :$column";
        }
    }

    /**
     * Filter and export send messages to export
     * @param array $messages
     * @param bool $final
     * @throws \yii\db\Exception
     */
    public function collect($messages, $final = true)
    {
        $this->messages = ArrayHelper::merge($this->messages, $this->filteringMessages($messages));
        $count = count($this->messages);
        if ($count > 0 and ($final or ($this->exportInterval > 0 and $count >= $this->exportInterval))) {
            $this->export();
            $this->messages = [];
        }
    }

    /**
     * Check is set column in message and convert array or object to json
     * @param $message
     * @param $columnName
     * @return false|null|string
     */
    public function getColumnValue($message, $columnName)
    {
        if (!isset($message['columns'][$columnName])) {
            return null;
        }

        return $this->properties->getValueForWrite($columnName, $message['columns'][$columnName]);
    }

    /**
     * @param $messages
     * @return mixed
     */
    public function filteringMessages($messages)
    {
        foreach ($messages as $key => $message) {
            if (!isset($message['category']) or !(in_array($message['category'], $this->categories) or empty($this->categories))) {
                unset($messages[$key]);
            }
        }

        return $messages;
    }

    /**
     * @throws \yii\db\Exception
     */
    public function rotate()
    {
        // Check if date_time field is present
        if (!in_array('date_time', $this->getColumns())) {
            return;
        }
        $tableName = $this->db->quoteTableName($this->logTable);
        $sql = "DELETE FROM $tableName WHERE date_time <= :date_time";
        $bindValues = [
            ":date_time" => date("Y-m-d H:i:s", time() - $this->timeToStoreLogs),
        ];
        $command = $this->db->createCommand($sql);
        $command->bindValues($bindValues)->execute();
    }
}
