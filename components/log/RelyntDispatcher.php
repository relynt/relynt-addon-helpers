<?php

namespace relynt\components\log;

use Exception;
use relynt\components\log\targets\RelyntDbTarget;
use yii\base\ErrorHandler;
use yii\log\Dispatcher;
use Yii;
use yii\log\Logger;

/**
 * Class RelyntDispatcher
 * @package relynt\components\log
 */
class RelyntDispatcher extends Dispatcher
{
    public function __construct(RelyntLogger $logger, array $config = [])
    {
        Yii::setLogger($logger);
        parent::__construct($config);
    }
    /**
     * @inheritDoc
     */
    public function dispatch($messages, $final)
    {
        $targetErrors = [];
        foreach ($this->targets as $target) {
            if ($target->enabled) {
                try {
                    //fix for other targets
                    $messagesForTarget = $messages;
                    if (!($target instanceof RelyntDbTarget)) {
                        $messagesForTarget = array_filter($messages, function ($message) {
                            return isset($message[1]);
                        });
                    }

                    $target->collect($messagesForTarget, $final);
                } catch (Exception $e) {
                    $target->enabled = false;
                    $targetErrors[] = [
                        'Unable to send log via ' . get_class($target) . ': ' . ErrorHandler::convertExceptionToString($e),
                        Logger::LEVEL_WARNING,
                        __METHOD__,
                        microtime(true),
                        [],
                    ];
                }
            }
        }

        if (!empty($targetErrors)) {
            $this->dispatch($targetErrors, true);
        }
    }

    /**
     * @throws \yii\db\Exception
     */
    public function rotate()
    {
        foreach ($this->targets as $target) {
            if ($target->enabled and $target instanceof RelyntDbTarget) {
                $target->rotate();
            }
        }
    }
}
