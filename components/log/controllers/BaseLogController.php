<?php

namespace relynt\components\log\controllers;

use Exception;
use relynt\components\log\models\LogProperty;
use relynt\components\log\models\Logs;
use relynt\helpers\DateHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class BaseLogController
 *
 * You may extends from this controller your log controller if you want use default views
 * @package relynt\components\log\controllers
 */
class BaseLogController extends Controller
{
    /** @var string namespace to Logs model */
    public static $logModel = 'relynt\components\log\models\Logs';

    public $layout = "@app/../relynt-addon-base-2/vendor/relynt/relynt-addon-helpers/components/log/views/layout";

    public $serverSideLimit = 1000;

    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return '/var/www/relynt/addons/relynt-addon-base-2/vendor/relynt/relynt-addon-helpers/components/log/views';
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Action index
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        /** @var Logs $model */
        $model = Yii::createObject(static::$logModel);
        $dates = [];
        foreach ($model->getTarget()->properties->getPropertiesParams() as $name => $params) {
            if (isset($params['type']) and $params['type'] == LogProperty::TYPE_DATETIME) {
                $dates[$name] = Inflector::humanize($name);
            }
        }

        return $this->render('log', [
            'dates' => $dates,
            'model' => $model,
        ]);
    }

    /**
     * Action get-logs-records
     * @return array
     * @throws InvalidConfigException
     */
    public function actionGetLogsRecords()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Yii::createObject(static::$logModel);
        $count = $model->find()->count();

        if ($count <= $this->serverSideLimit) {
            return [
                'data' => $model->getItemsForDataTable()
            ];
        }

        return [
            'data' => $model->getServerSideData(),
            'draw' => 0,
            'recordsTotal' => $count,
            'recordsFiltered' => $model->getServerSideCount(),
            'serverSideEnabled' => true,
        ];
    }


    /**
     * Dummy for action clear-log
     * @return array
     */
    public function actionClearLog()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'response' => true,
            'message' => Yii::t('app', "Old log records will be cleared automatically"),
        ];
    }

    /**
     * Action get-record
     * @param integer $id
     * @return string
     * @throws Exception
     */
    public function actionGetLogRecord($id)
    {
        /** @var Logs $logModel */
        $logModel = new static::$logModel();

        return $this->render('modal', [
            'record' => $logModel::getSingleRecord($id),
            'propertyParams' => $logModel->getPropertyParams(),
            'model' => $logModel,
        ]);
    }
}
