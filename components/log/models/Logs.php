<?php

namespace relynt\components\log\models;

use Exception;
use relynt\components\log\RelyntLogger;
use relynt\components\log\targets\RelyntDbTarget;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * Class Logs
 * @package relynt\components\log\models
 */
class Logs extends ActiveRecord
{
    public static $tableName = '{{%log}}';

    /** @var array */
    protected static $logRecords;

    /** @var int */
    private $_counterNestedItems = 0;

    /** @var RelyntDbTarget */
    protected $_target;

    protected $propertiesParams;

    private $_hasHideInDetails;

    private $_existActions = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setTarget();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return static::$tableName;
    }

    /**
     * Return base properties
     * @return array
     */
    public function getDefaultProperties()
    {
        return [
            'id',
            'message',
            'status',
        ];
    }

    /**
     * Return properties from target config if this is relynt target
     * @return array
     * @throws Exception
     */
    public function getPropertiesFromTarget()
    {
        if (!empty($this->_target)) {
            return $this->getTarget()->properties->getPropertyNames();
        }

        return [];
    }

    /**
     * @param null $target
     */
    public function setTarget($target = null)
    {
        if (!empty($target)) {
            $this->_target = $target;
            return;
        }

        $this->_target = $this->findTarget();
    }

    /**
     * @return RelyntDbTarget
     * @throws Exception
     */
    public function getTarget()
    {
        if (empty($this->_target)) {
            $this->setTarget();
            if (empty($this->_target)) {
                throw new Exception(Yii::t('app', "Target not found."));
            }
        }

        return $this->_target;
    }

    /**
     * @return \yii\log\Target|null
     */
    protected function findTarget()
    {
        foreach (\Yii::$app->log->targets as $target) {
            if (isset($target->logTable) and $target->logTable == static::$tableName and isset($target->properties)) {
                return $target;
            }
        }

        return null;
    }

    /**
     * Return all attributes
     * @return array
     * @throws Exception
     */
    public function getAttributesForList()
    {
        return ArrayHelper::merge($this->getDefaultProperties(), $this->getPropertiesFromTarget());
    }

    /**
     * Generate and return DataTable header
     * @return string
     * @throws Exception
     */
    public function generateCodeForTableHead()
    {
        $html = '';
        $attributes = $this->getAttributesForList();

        foreach ($attributes as $attribute) {
            if ($this->isHideInDetails($attribute)) {
                continue;
            }

            $html .= Html::tag('th', $this->getAttributeLabel($attribute));
        }

        if ($this->checkExistActions()) {
            $html .= Html::tag('th', Yii::t('app', 'Actions'));
        }

        return $html;
    }

    /**
     * Return all log record from database
     * @return array|ActiveRecord[]
     */
    public function getAllLogRecords()
    {
        if (static::$logRecords === null) {
            static::$logRecords = static::find()->asArray()->all();
        }

        return static::$logRecords;
    }

    /**
     * Convert all log records form object to array
     * @return array
     * @throws Exception
     */
    public function getItemsForDataTable()
    {
        return $this->prepareData();
    }

    /**
     * Return table column names for DataTable
     * @return false|string
     * @throws Exception
     */
    public function getTableColumnNames()
    {
        $columns = [];

        foreach ($this->getAttributesForList() as $attribute) {
            if (!$this->isHideInDetails($attribute)) {
                $columns[] = ['data' => $attribute, 'name' => $attribute];
            }
        }

        if ($this->checkExistActions()) {
            $columns[] = ['data' => 'logActions'];
        }

        return json_encode($columns);
    }

    /**
     * Clear log table
     * @return int
     */
    public function clearAll()
    {
        return static::deleteAll();
    }

    /**
     * Prepare data before print
     * @return array
     * @throws Exception
     */
    public function prepareData()
    {
        foreach ($this->getAllLogRecords() as $index => $record) {
            static::$logRecords[$index] = $this->prepareRecord($record);
        }

        return static::$logRecords;
    }

    public function getServerSideData()
    {
        $logRecords = $this->getRequestForServerSide()->asArray()->all();
        foreach ($logRecords as $index => $record) {
            $logRecords[$index] = $this->prepareRecord($record);
        }
        return $logRecords;
    }

    public function getServerSideCount()
    {
        return $this->getRequestForServerSide()->count();
    }

    public function getRequestForServerSide()
    {
        $get = Yii::$app->getRequest()->get();
        $query = static::find()
            ->offset($get['start'])
            ->limit($get['length']);

        if (isset($get['order']) and !empty($get['order'])) {
            $columns = json_decode($this->getTableColumnNames(), 1);
            $orders = [];
            foreach ($get['order'] as $order) {
                if (isset($columns[$order['column']]) and $columns[$order['column']]['data'] != 'logActions') {
                    $orders[$columns[$order['column']]['data']] = $order['dir'] === 'desc' ? SORT_DESC : SORT_ASC;
                }
            }

            $query->orderBy($orders);
        }

        $params = $this->getTarget()->properties->getPropertiesParams();
        foreach (($get['columns']) as $column) {
            $value = $column['search']['value'];
            if (!empty($value)) {
                $name = $column['data'];

                if (isset($params[$name]) and $params[$name]['type'] == LogProperty::TYPE_DATETIME) {
                    $value = explode(' - ', $value);
                    $whereCondition = [
                        'and'
                    ];
                    $whereCondition[] = ['BETWEEN', $name, $value[0] . " 00:00:00", $value[1] . " 23:59:59"];
                    $query->andWhere($whereCondition);
                } else {
                    $query->andWhere(['LIKE', $name, '%' . $value . '%', false]);
                }
            }
        }

        if (!empty($get['search']) and !empty($get['search']['value'])) {
            $attributesForList = $this->getAttributesForList();
            $whereCondition = [
                'or'
            ];
            foreach ($attributesForList as $attributes) {
                $whereCondition[] = ['LIKE', $attributes, '%' . $get['search']['value'] . '%', false];
            }
            $query->andWhere($whereCondition);
        }

        return $query;
    }

    /**
     * @param array $record
     * @param bool $details
     * @return array
     * @throws Exception
     */
    public function prepareRecord($record, $details = false)
    {
        foreach ($record as $key => $item) {
            if (!$details and $this->isHideInDetails($key)) {
                unset($record[$key]);
                continue;
            }

            $record[$key] = $this->prepareValue($key, $item, $details);
        }

        $actions = $this->getActions($record);
        if (!$details and !empty($actions)) {
            $record['logActions'] = implode(' ', $actions);
        }

        return $record;
    }

    /**
     * Print array
     * @param $array
     * @param string $key
     * @param string $prefix
     * @return string
     */
    public function printArray($array, $key = '', $prefix = '')
    {
        if (count($array) > 5) {
            $array = [
                Inflector::humanize($key) => $array,
            ];
        }

        $html = "<ul class='sidebar-nav' style='padding-left: 10px'>";
        $html .= $this->getArrayView($array, $prefix);
        $html .= '</ul>';
        return '<div>' . $html . '</div>';
    }

    /**
     * Recursive build list
     * @param $prefix
     * @param $array
     * @return string
     */
    protected function getArrayView($array, $prefix = '')
    {
        $arrayHtml = '';
        foreach ($array as $key => $value) {
            $keyValue = is_numeric($key) ? ($key + 1) : $key;
            if (is_array($value)) {
                $value = $this->getArrayView($value, $prefix);
                $this->_counterNestedItems++;
                $arrayHtml .= "
                <li style='list-style-type: none;'>
                    <a href='#{$prefix}-submenu-{$this->_counterNestedItems}' class='accordion-toggle collapsed toggle-switch list-link' data-toggle='collapse'>
                        <span><strong>$keyValue: </strong></span>
                        <b class='caret'></b>
                    </a>
                    <ul id='{$prefix}-submenu-{$this->_counterNestedItems}' class='panel-collapse collapse panel-switch' role='menu'>
                        $value
                    </ul>
                </li>
            ";
            } else {
                $arrayHtml .= "<li style='list-style-type: none;'><a class='list-link'><span><strong>$keyValue:</strong>  $value</span></a></li>";
            }
        }
        return $arrayHtml;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $details
     * @return string
     * @throws Exception
     */
    public function prepareValue($attribute, $value, $details = false)
    {
        $params = $this->getTarget()->properties->getPropertyParams($attribute);

        if ($attribute == 'status' and $this->getTarget()->prettyStatus) {
            return $this->getStatusLabel($value);
        }

        if (empty($params)) {
            $result = json_decode($value, true);
            if ($result != null and is_array($result)) {
                if ($details) {
                    return $this->printArray($result, $attribute, 'details');
                }

                return $this->printArray($result, $attribute);
            }
            return $value;
        }

        if (isset($params[LogProperty::OUTPUT_CALLBACK])) {
            return $params[LogProperty::OUTPUT_CALLBACK]($value);
        }

        if (!empty($params[LogProperty::TYPE])) {
            return $this->prepareValueByType($params[LogProperty::TYPE], $value, $attribute, $details);
        }

        return $value;
    }

    /**
     * Prepare value for output by type
     * @param $type
     * @param $value
     * @param $attribute
     * @param $details
     * @return mixed|string
     */
    public function prepareValueByType($type, $value, $attribute, $details = false)
    {
        switch ($type) {
            case LogProperty::TYPE_ARRAY:
            case LogProperty::TYPE_OBJECT:
                $value = json_decode($value, true);
                if (!empty($value)) {
                    if (is_array($value)) {
                        if ($details) {
                            return $this->printArray($value, $attribute, 'details');
                        }

                        return $this->printArray($value, $attribute);
                    }

                    return Yii::t('app', "Error while preparing data to display.");
                }

                return '';
            default:
                return $value;
        }
    }

    /**
     * @return bool if true exist actions (used for add column actions into datatable)
     * @throws Exception
     */
    public function checkExistActions()
    {
        if ($this->_existActions === null) {
            $this->_existActions = false;
            if ($this->hasHideInDetails() or $this->getTarget()->enableActions) {
                $this->_existActions = true;
            }
        }

        return $this->_existActions;
    }

    /**
     * @param array $model
     * @return array
     * @throws Exception
     */
    public function getActions($model)
    {
        return ArrayHelper::merge($this->logActions($model), $this->getDefaultLogActions($model));
    }

    /**
     * Override this for add custom actions
     *
     * Example: return [
     *      'test_action' => Html::a("Test action", 'example/link'),
     * ];
     * @param array $model
     * @return array
     */
    public function logActions($model)
    {
        return [];
    }

    /**
     * @param array $model
     * @return array array of default actions
     * @throws Exception
     */
    protected function getDefaultLogActions($model)
    {
        $actions = [];
        if ($this->hasHideInDetails()) {
            $actions['details_action'] = Html::a("<span class='glyphicon glyphicon-eye-open'></span>", '#', [
                'onclick' => "showDetailsModal({$model['id']})"
            ]);
        }

        return $actions;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function hasHideInDetails()
    {
        if ($this->_hasHideInDetails === null) {
            $this->_hasHideInDetails = false;
            foreach ($this->getTarget()->properties->getPropertyNames() as $propertyName) {
                if ($this->isHideInDetails($propertyName)) {
                    $this->_hasHideInDetails = true;
                }
            }
        }

        return $this->_hasHideInDetails;
    }

    /**
     * @return array properties params
     * @throws Exception
     */
    public function getPropertyParams()
    {
        if ($this->propertiesParams === null) {
            $this->propertiesParams = $this->getTarget()->properties->getPropertiesParams();
        }

        return $this->propertiesParams;
    }

    /**
     * @param string $propertyName
     * @param string $parameterName
     * @return mixed|null
     * @throws Exception
     */
    public function getPropertyParameter($propertyName, $parameterName)
    {
        $parameters = $this->getPropertyParams();
        return isset($parameters[$propertyName][$parameterName]) ? $parameters[$propertyName][$parameterName] : null;
    }

    /**
     * check if in property config set HIDE_INTO_DETAILS_VIEW
     * @param $propertyName
     * @return bool|null
     * @throws Exception
     */
    public function isHideInDetails($propertyName)
    {
        return $this->getPropertyParameter($propertyName, LogProperty::HIDE_INTO_DETAILS_VIEW);
    }

    /**
     * Get and prepare record by id
     * @param integer $id
     * @return array|ActiveRecord|null
     */
    public static function getSingleRecord($id)
    {
        $record = static::find()->where(['id' => $id])->asArray()->one();

        if (empty($record)) {
            return $record;
        }

        return (new static())->prepareRecord($record, true);
    }

    /**
     * Need override for add custom status labels
     * @return array
     */
    public function statusesLabels()
    {
        return [];
    }

    public static $labelsCache;

    /**
     * @param $value
     * @return string
     */
    public function getStatusLabel($value)
    {
        // ask if clear cash after update add-on
        if (static::$labelsCache === null) {
            static::$labelsCache = ArrayHelper::merge([
                RelyntLogger::STATUS_SUCCESS => '<span class="label label-success">' . Yii::t('app', 'Success') . '</span>',
                RelyntLogger::STATUS_FAILED => '<span class="label label-danger">' . Yii::t('app', 'Failed') . '</span>',
            ], $this->statusesLabels());
        }

        return isset(static::$labelsCache[$value]) ? static::$labelsCache[$value] : $value;
    }
}
