<?php

namespace relynt\components\log\models;

use yii\base\Model;

/**
 * Class LogProperty
 * @package relynt\components\log\models
 */
class LogProperty extends Model
{
    const TYPE_ARRAY = 'array';
    const TYPE_OBJECT = 'object';
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_DATETIME = 'datetime';

    /**
     * Properties for target config
     */
    const TYPE = 'type'; // configure type for property
    const HIDE_INTO_DETAILS_VIEW = 'in_details'; // set flag about hide from table and show in details modal window
    const DETAILS_VIEW_SHOW_IN_ADDITIONAL_PANEL = 'show_in_additional_panel'; // show value in additional panel in details modal window
    const OUTPUT_CALLBACK = 'output_callback'; // callback for show value
    const INPUT_CALLBACK = 'input_callback'; // callback for write value

    /** @var $property array */
    public $property;

    /** @var $_propertyNames array */
    private $_propertyNames;

    public $propertyParams;

    /**
     * Prepare value for write to db
     * @param $property
     * @param $value
     * @return false|string
     */
    public function getValueForWrite($property, $value)
    {
        $params = $this->getPropertyParams($property);
        if (!empty($params)) {
            return $this->prepareValue($params, $value);
        } else {
            if (is_array($value) or is_object($value)) {
                return json_encode($value);
            }
        }

        return $value;
    }

    /**
     * @return array of property names
     */
    public function getPropertyNames()
    {
        if ($this->_propertyNames === null) {
            $this->_propertyNames = [];
            foreach ($this->property as $key => $item) {
                $this->_propertyNames[] = is_array($item) ? $key : $item;
            }
        }
        return $this->_propertyNames;
    }

    /**
     * @param $name
     * @return array property params
     */
    public function getPropertyParams($name)
    {
        return isset($this->property[$name]) ? $this->property[$name] : [];
    }

    /**
     * Prepare value
     * @param $params
     * @param $value
     * @return false|string
     */
    public function prepareValue($params, $value)
    {
        if (isset($params[static::INPUT_CALLBACK])) {
            return $params[static::INPUT_CALLBACK]($value);
        }

        if (isset($params[static::TYPE])) {
            return $this->prepareByValueType($params[static::TYPE], $value);
        }

        return $value;
    }

    /**
     * Prepare values by type
     * @param $type
     * @param $value
     * @return false|string
     */
    public function prepareByValueType($type, $value)
    {
        switch ($type) {
            case static::TYPE_ARRAY:
            case static::TYPE_OBJECT:
                return json_encode($value);
            default:
                return $value;
        }
    }

    /**
     * @return array with properties params
     */
    public function getPropertiesParams()
    {
        if ($this->propertyParams === null) {
            $this->propertyParams = [];
            foreach ($this->getPropertyNames() as $name) {
                $this->propertyParams[$name] = $this->getPropertyParams($name);
            }
        }

        return $this->propertyParams;
    }
}
