<?php

namespace relynt\components\log;

/**
 * Class Logger
 * @package relynt\components\log
 */
class RelyntLogger extends \yii\log\Logger
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';

    /**
     * @param string $message log message
     * @param array $columns array with your colymns where key this is name column in your database
     * @param string $status
     * @param string $category
     */
    public function writeToLog($message, $columns, $status, $category = 'application')
    {
        $this->messages[] = [
            'message' => $message,
            'columns' => $columns,
            'status' => $status,
            'category' => $category,
        ];

        if ($this->flushInterval > 0 and count($this->messages) >= $this->flushInterval) {
            $this->flush();
        }
    }

    /**
     * Write logs with success status
     *
     * @param string $message
     * @param array $columns array where key this is name column in your table and value it is value your field
     *          example: [
     *              'user_id' => 6,
     *              'error' => 'Error message'
     *          ]
     * @param string $category
     */
    public function success($message, $columns = [], $category = 'application')
    {
        $this->writeToLog($message, $columns, static::STATUS_SUCCESS, $category);
    }

    /**
     * Write logs with failed status
     *
     * @param string $message
     * @param array $columns array where key this is name column in your table and value it is value your field
     *          example: [
     *              'user_id' => 6,
     *              'error' => 'Error message'
     *          ]
     * @param string $category
     */
    public function failed($message, $columns = [], $category = 'application')
    {
        $this->writeToLog($message, $columns, static::STATUS_FAILED, $category);
    }

    /**
     * @param bool $final
     */
    public function flush($final = false)
    {
        parent::flush($final);
        if ($final && method_exists($this->dispatcher, 'rotate')) {
            $this->dispatcher->rotate();
        }
    }
}
