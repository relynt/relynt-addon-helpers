<?php

namespace relynt\components\log\migrations;

use yii\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class BaseMigrationForLogTable
 *
 * If you want use custom field in you log, you mast create migration and extends her from this class.
 * For data type array you mast create field type string. RelyntDbTarget convert array to json.
 *
 * @package relynt\components\log\migrations
 */
class BaseMigrationForLogTable extends Migration
{
    public function create($tableName = "{{%log}}", $columns = [])
    {
        return $this->createTable($tableName, ArrayHelper::merge([
            'id' => $this->primaryKey(),
            'message' => $this->string(),
            'status' => $this->string(),
        ], $columns));
    }
}