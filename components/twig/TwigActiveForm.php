<?php

namespace relynt\components\twig;

use Twig\TwigFunction;
use yii\bootstrap\ActiveForm;
use yii\twig\Extension;

class TwigActiveForm extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ActiveForm';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('form_begin', [$this, 'begin']),
            new TwigFunction('form_end', [$this, 'end']),
        ];
    }

    public function begin($args)
    {
        return ActiveForm::begin($args);
    }

    public function end()
    {
        ActiveForm::end();
    }
}
