<?php

namespace relynt\components\twig;

use Twig\TwigFunction;
use yii\twig\Extension;
use yii\widgets\Pjax;

class TwigPjax extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Pjax';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('pjax_begin', [$this, 'begin']),
            new TwigFunction('pjax_end', [$this, 'end'])
        ];
    }

    public function begin($args = [])
    {
        return Pjax::begin($args);
    }

    public function end()
    {
        Pjax::end();
    }
}
