<?php

namespace relynt\components\tasks\migrations;

use yii\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class BaseMigrationForTask
 *
 * If you want use custom field in you task manager, you mast create migration and extends her from this class.
 *
 * @package relynt\components\tasks\migrations
 */
class BaseMigrationForTask extends Migration
{
    public function create($tableName = "{{%tasks}}", $fields = [])
    {
        return $this->createTable($tableName, ArrayHelper::merge([
            'id' => $this->primaryKey(),
            'start_time' => $this->string(),
            'update_time' => $this->string(),
            'status' => $this->string(),
            'worker_class' => $this->string(),
            'arguments' => $this->string(),
            'pid' => $this->string(),
            'progress' => $this->double(),
            'errors' => $this->text(),
        ], $fields));
    }
}