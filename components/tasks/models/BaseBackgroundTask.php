<?php

namespace relynt\components\tasks\models;

use Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class BackgroundTask
 *
 * @package relynt\components\tasks\models
 *
 * @property integer $id
 * @property string $start_time
 * @property string $update_time
 * @property string $status
 * @property string $worker_class
 * @property string $arguments
 * @property double $progress
 * @property string $errors
 * @property string $pid
 */
class BaseBackgroundTask extends ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_PROCESSING = 'processing';
    const STATUS_DONE = 'done';
    const STATUS_ERROR = 'error';
    const STATUS_DONE_WITH_ERROR = 'done-with-error';
    const STATUS_DOWN = 'fell-down';

    /** @var  BaseWorker _worker */
    private $_worker;

    /**
     * Override if you use some else class name for run tasks in background
     *
     * Example: protected $controller = 'same/vay/controller-id';
     *
     * @var string
     */
    protected $controller = 'process';

    /**
     * Override if you use some else action name for run tasks in background
     *
     * Example: protected $action = 'run-process-in-background'
     *
     * @var string
     */
    protected $action = 'run-in-background';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return "{{%tasks}}";
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['start_time', 'update_time', 'status', 'worker_class', 'arguments'], 'string'],
            [['progress'], 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge($this->getBaseAttributesLabels(), $this->getAttributesLabels());
    }

    /**
     * Override and return array attributes labels
     * @return array
     */
    public function getAttributesLabels()
    {
        return [];
    }

    /**
     * @return array labels base attributes
     */
    private function getBaseAttributesLabels()
    {
        return [
            "id" => Yii::t('app', "ID"),
            "start_time" => Yii::t('app', "Start time"),
            "update_time" => Yii::t('app', "Update time"),
            "status" => Yii::t('app', "Status"),
            "progress" => Yii::t('app', "Progress"),
        ];
    }

    /**
     * @return array attributes this model
     */
    public function getAttributesForList()
    {
        return [
            'id',
            'start_time',
            'update_time',
            'status',
            'progress',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_time'],
                    ActiveRecord::EVENT_BEFORE_INSERT => ['start_time'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * Create task in task manager
     * @param $workerClass
     * @param array $arguments
     * @return bool|static
     */
    public static function create($workerClass, $arguments = [])
    {
        $model = new static();
        $model->worker_class = $workerClass;
        $model->status = static::STATUS_NEW;
        $model->progress = 0;
        $model->setArguments($arguments);

        if (!$model->save()) {
            return false;
        }

        return $model;
    }

    /**
     * Run task if his have valid property
     * @param $processId
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function runProcess($processId)
    {
        $process = static::findOne(['id' => $processId]);
        if (empty($process)) {
            return static::getErrorResponse(Yii::t('app', "Process with id: {id} is not found.", [
                'id' => $processId
            ]));
        }

        if ($process->isRunningProcess()) {
            return static::getErrorResponse(Yii::t('app', "Process with id: {id} already started.", [
                'id' => $processId
            ]));
        }

        if ($process->status == static::STATUS_DONE) {
            return static::getErrorResponse(Yii::t('app', "Process with id: {id} already done.", [
                'id' => $processId
            ]));
        }

        if (!class_exists($process->worker_class)) {
            return static::getErrorResponse(Yii::t('app', "Worker class {class} was not exist.", [
                'class' => $process->worker_class
            ]));
        }

        $process->setWorker();

        return $process->runWorker();
    }

    /**
     * Run process in background
     * @param null $logFile
     */
    public function runBackgroundProcess($logFile = null)
    {
        $logs = '/dev/null';
        if (!empty($logFile)) {
            $logs = Yii::getAlias($logFile);
        }

        exec(Yii::$app->getBasePath() . "/yii {$this->controller}/{$this->action} --processId={$this->id} >> $logs 2>&1 & echo $!", $pid);

        $pid = (empty($output) and !is_array($pid)) ? null : reset($pid);
        if (!empty($pid)) {
            $this->pid = $pid;
            $this->save();
        }
    }

    /**
     * Check is running process
     * @return bool
     */
    public function isRunningProcess()
    {
        return $this->status == self::STATUS_PROCESSING and !empty($this->pid) and file_exists("/proc/$this->pid");
    }

    /**
     * Create error response
     * @param string $message
     * @return array
     */
    public static function getErrorResponse($message = '')
    {
        return [
            'result' => false,
            'message' => $message,
        ];
    }

    /**
     * Create success response
     * @param string $message
     * @return array
     */
    public static function getSuccessResponse($message = '')
    {
        return [
            'result' => true,
            'message' => $message,
        ];
    }

    /**
     * Set status for process
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    /**
     * Update progress
     * @param $progress
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function updateProgress($progress)
    {
        $this->getWorker()->onProgress($progress);
        $this->progress += $progress;
        return $this->save();
    }

    /**
     * Get task by her id
     * @param $processId
     * @return BaseBackgroundTask|null
     */
    public static function getProcess($processId)
    {
        return static::findOne(['id' => $processId]);
    }

    /**
     * Set worker
     * @throws \yii\base\InvalidConfigException
     */
    public function setWorker()
    {
        $this->_worker = Yii::createObject($this->worker_class);
        $this->_worker->arguments = $this->getArguments();
        $this->_worker->process = $this;
    }

    /**
     * Get worker
     * @return BaseWorker|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getWorker()
    {
        if ($this->_worker === null) {
            $this->setWorker();
        }

        return $this->_worker;
    }

    /**
     * Run task in worker
     * @return array
     */
    public function runWorker()
    {
        $this->setStatusProcessing();

        $message = '';
        try {
            $processResult = $this->_worker->run();
        } catch (Exception $exception) {
            $processResult = false;
            $message = Yii::t('app', "Exception {exception}\n File: {file}:{line}", [
                'exception' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);
        }

        if ($this->_worker->hasErrors()) {
            $this->setWorkerErrors();
        }

        if ($processResult) {
            if ($this->_worker->hasErrors()) {
                $message = Yii::t('app', "Process done with errors.");
                $this->setStatusDoneWithError();
                $this->_worker->onDoneWithErrors();
            } else {
                $message = Yii::t('app', "Process successfully ended.");
                $this->setStatusDone();
                $this->_worker->onSuccess();
            }

            return static::getSuccessResponse($message);
        }

        $this->setStatusError();
        $this->_worker->onError();
        return static::getErrorResponse($message);
    }

    /**
     * Set arguments
     * @param array $arguments
     */
    public function setArguments($arguments)
    {
        $this->arguments = json_encode($arguments);
    }

    /**
     * Get arguments
     * @return mixed
     */
    public function getArguments()
    {
        return json_decode($this->arguments, true);
    }

    /**
     * Set errors
     */
    public function setWorkerErrors()
    {
        $this->errors = json_encode($this->_worker->getErrors());
    }

    /**
     * Get task errors
     * @return mixed
     */
    public function getWorkerErrors()
    {
        return json_decode($this->errors, true);
    }

    /**
     * Set status processing
     */
    public function setStatusProcessing()
    {
        $this->setStatus(static::STATUS_PROCESSING);
    }

    /**
     * Set status done
     */
    public function setStatusDone()
    {
        $this->progress = 100;
        $this->setStatus(static::STATUS_DONE);
    }

    /**
     * Set status error
     */
    public function setStatusError()
    {
        $this->setStatus(static::STATUS_ERROR);
    }

    /**
     * Set status done-with-error
     */
    public function setStatusDoneWithError()
    {
        $this->progress = 100;
        $this->setStatus(static::STATUS_DONE_WITH_ERROR);
    }

    /**
     * Get task status and update fer for fall-down if process not running
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getStatus()
    {
        if ($this->status == static::STATUS_PROCESSING and !$this->isRunningProcess()) {
            $this->getWorker()->onDown();
            $this->status = static::STATUS_DOWN;
            $this->save();
        }

        return $this->status;
    }

    /**
     * Kill running process
     */
    public function kill()
    {
        exec("kill $this->pid");
    }
}
