<?php

namespace relynt\components\tasks\models;

use yii\base\Model;

/**
 * Class BaseWorker processing long work
 *
 * @package relynt\components\tasks\models
 */
abstract class BaseWorker extends Model
{
    /** @var array */
    public $arguments;

    /** @var BaseBackgroundTask */
    public $process;

    private $_progress;
    private $_result;

    /**
     * @return mixed
     */
    abstract public function run();

    /**
     * Set result
     * @param $result
     */
    public function setResult($result)
    {
        $this->_result = $result;
    }

    /**
     * Get result
     * @return mixed
     */
    public function getResult()
    {
        return $this->_result;
    }

    /**
     * Callback for success result working process
     */
    public function onSuccess()
    {
    }

    /**
     * Callback for error result working process
     */
    public function onError()
    {
    }

    /**
     * Callback for processing
     * @param $progress
     */
    public function onProgress($progress)
    {
        $this->_progress = $progress;
    }

    /**
     * Callback for done with errors result working process
     */
    public function onDoneWithErrors()
    {
    }

    /**
     * Callback for down status
     */
    public function onDown()
    {
    }
}
