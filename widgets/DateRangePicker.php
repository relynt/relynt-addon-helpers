<?php

namespace relynt\widgets;

use yii\helpers\Html;
use relynt\assets\DataRangePickerAsset;
use yii\widgets\InputWidget;

/**
 * Class DateRangePicker
 * @package relynt\widgets
 *
 * Widget for using DateRangePicker jQuery plugin. Usage example:
 *
 * DateRangePicker::widget([
 *      'name' => "DateRangePickerNameInput",
 *      'id' => 'DateRangePickerIdInput',
 *      'value' => 'value',
 *      'options' => [
 *          'data-attr' => 'SomeData',
 *      ],
 *      'pluginOptions' => [
 *          'locale' => [
 *              'format' => DateHelper::getJsRelyntDateFormat(),
 *              'firstDay' => DateHelper::getRelyntFirstDayOfTheWeek(),
 *          ],
 *          'singleDatePicker'=> true,
 *      ],
 *      'callback'=>'function callback(){}'
 * ])
 */
class DateRangePicker extends InputWidget
{
    const DEFAULT_INPUT_CLASSES = 'form-control input-sm date-range-picker-input';

    /**
     * Template for HTML <input> tag
     */
    public $template = '<input id=":id" value=":value" name=":name" class=":class" :options>';

    /**
     * Additional class names for HTML <input> tag
     * @var string Example: 'form-control input-sm'
     */
    public $class = '';

    /**
     * Id for Html <select> tag, needs for init Select2 Plugin by this id, will be used in function initSelect2()
     * @var string
     */
    public $id = 'dateRangePicker';

    /**
     * Array of options for jQuery daterangepicker Plugin,
     * Can be used any api options from @url https://www.daterangepicker.com/#options
     */
    public $pluginOptions = [];
    public $callback = false;

    public function init()
    {
        DataRangePickerAsset::register($this->getView());
        parent::init();
    }

    public function run()
    {
        $options = json_encode([
            'id' => $this->id,
            'pluginOptions' => $this->pluginOptions
        ]);
        echo $this->renderInput();
        $this->getView()->registerJs('initDateRangePicker(' . $options . ')');
    }

    public function renderInput(): string
    {
        $search = [':id', ':value', ':name', ':class', ':options'];
        $replace = [$this->id, $this->value, $this->name, static::DEFAULT_INPUT_CLASSES . $this->class, Html::renderTagAttributes($this->options)];
        return str_replace($search, $replace, $this->template);
    }
}
