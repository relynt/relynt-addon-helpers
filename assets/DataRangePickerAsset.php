<?php

namespace relynt\assets;

use yii\web\AssetBundle;

class DataRangePickerAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $js = [
        'js/moment.min.js',
        'js/daterangepicker.min.js',
        'js/initDateRangePicker.js'
    ];

    public $css = [
        'css/daterangepicker.css',
        'css/customDateRangePicker.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
