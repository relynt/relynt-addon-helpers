<?php

namespace relynt\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $js = [
        'js/select2/select2.min.js',
        'js/select2/initSelect2.js',
    ];

    public $css = [
        'css/select2/select2.min.css',
        'css/select2/customSelect2.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
