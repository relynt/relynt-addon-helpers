function initSelect2(options) {
    let id = options.id;
    let value = options.value;
    let placeholder = options.placeholder;


    let defaultSelectOptions = {
        'placeholder': placeholder,
    }

    let selectOptions = $.extend(true, defaultSelectOptions, options.pluginOptions);
    let select2Input = $('#' + id).select2(selectOptions);


    if (options.pluginOptions.ajax && Object.keys(options.pluginOptions.ajax).length != 0) {
        if (typeof value === 'string') {
            let newOption = new Option(value, value, false, false);
            $('#' + id).append(newOption).trigger('change');
        } else if (typeof value === 'object') {
            for (let k in value) {
                let newOption = new Option(value[k], value[k], false, true);
                $('#' + id).append(newOption).trigger('change');
            }
        }
    } else {
        if (typeof value === 'string') {
            $('#' + id).val(value).trigger('change');
        }
    }

    if (options.callback) {
        let callbackSelect2 = '(' + options.callback + ')';
        let functionCallbackSelect2 = eval(callbackSelect2);
        functionCallbackSelect2();
    }
}