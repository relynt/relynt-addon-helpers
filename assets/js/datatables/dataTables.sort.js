jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "ip-address-pre": function (a) {
        var m = String(a).replace(/<[\s\S]*?>/g, "").split("."), x = "";
        for (var i = 0; i < m.length; i++) {
            var item = m[i];
            if (item.length == 1) {
                x += "00" + item;
            } else if (item.length == 2) {
                x += "0" + item;
            } else {
                x += item;
            }
        }
        return x;
    },
    "ip-address-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    "ip-address-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "num-html-pre": function (a) {
        var x = String(a).replace(/<[\s\S]*?>/g, "");
        return parseFloat(x);
    },
    "num-html-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    "num-html-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});
jQuery.fn.dataTable.ext.type.order['file-size-pre'] = function (data) {
    if (data === '---') {
        return -1;
    }

    var units = data.replace(/[\d\.\s]/g, '').toLowerCase();
    var multiplier = 1;
    if (units === 'kb') {
        multiplier = 1000;
    }
    else if (units === 'mb') {
        multiplier = 1000000;
    }
    else if (units === 'gb') {
        multiplier = 1000000000;
    }
    else if (units === 'tb') {
        multiplier = 1000000000000;
    }
    else if (units === 'pb') {
        multiplier = 1000000000000000;
    }
    return parseFloat(data) * multiplier;
};
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function (a) {
        a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
        return parseFloat(a);
    },
    "currency-asc": function (a, b) {
        return a - b;
    },
    "currency-desc": function (a, b) {
        return b - a;
    }
});
