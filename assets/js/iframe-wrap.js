// fix for pages layout without iframe
$(function () {
    if (window.self == window.top) {
        $('body').wrapInner('<div class="wrapper" />');
    }
});