<?php

namespace relynt\assets;

use yii\web\AssetBundle;

/**
 * Class PaymentAddonAsset.php
 * For usage add this code to your AppAsset:
 *      public $depends = [
 *           'relynt\assets\PaymentAddonAsset',
 *      ];
 *
 * @package relynt
 */
class PaymentAddonAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $css = [
        'css/paymentAddon.css',
    ];

    public $js = [
        'js/iframe-wrap.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
