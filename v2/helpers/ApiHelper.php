<?php

namespace relynt\v2\helpers;

use relynt\base\CheckResponseTrait;
use relynt\helpers\RedisHelper;
use RelyntApi;
use yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Class ApiHelper provide functionality for working with Relynt API
 *
 * Save auth data to session
 *
 * ```php
 * $api = ApiHelper::getInstance();
 * $_SESSION['auth_data'] = $api->getAuthData();
 * ```
 *
 * Use saved to session auth data
 *
 * ```
 * $api = ApiHelper::getInstance();
 * $api->setAuthData($_SESSION['auth_data']);
 * ```
 *
 * @package relynt\helpers
 */
class ApiHelper extends BaseObject
{
    use CheckResponseTrait;

    /** @var RelyntApi */
    private $_api;

    /** @var string API base url */
    public $api_domain;

    /** @var static Instance of current class */
    private static $instance;

    private $_disableLogout = false;

    /** Name of header which contains amount of records */
    const HEADER_X_TOTAL_COUNT = 'X-total-count';

    /**
     * @param string $url API endpoint
     * @param bool $isSameServer
     * @return array
     * @throws InvalidConfigException
     */
    public static function checkApiSameServer($url = 'admin/api/check', $isSameServer = false)
    {
        $url .= '?checkAuth=true';
        if ($isSameServer) {
            $code = md5(microtime() . mt_rand());
            RedisHelper::setex('server_validating_codes_' . $code, 60, $code);
            $url .= '&code=' . $code;
        }
        $api = self::getInstance()->_api;
        if (!empty($api->response)) {
            if (!array_key_exists('error', $api->response)) {
                $api->api_call_get($url);
            }
        }
        return [
            'result' => $api->result,
            'response' => $api->response,
            'response_code' => $api->response_code
        ];
    }


    /**
     * @param string $url API endpoint
     * @return bool
     * @throws InvalidConfigException
     */
    public static function checkApi($url = 'admin/api/check')
    {
        $response = ApiHelper::checkApiSameServer($url);
        return $response['result'] && $response['response_code'] == 200;
    }

    /**
     * @return boolean
     */
    public function getResult()
    {
        if (self::$instance !== null && self::$instance->_api !== null) {
            return self::$instance->_api->result;
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getResponse()
    {
        if (self::$instance !== null && self::$instance->_api !== null) {
            return self::$instance->_api->response;
        }

        return null;
    }

    /**
     * @return integer|null
     */
    public function getResponseCode()
    {
        if (self::$instance !== null && self::$instance->_api !== null) {
            return self::$instance->_api->response_code;
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getResponseHeaders()
    {
        if (self::$instance !== null && self::$instance->_api !== null) {
            return self::$instance->_api->response_headers;
        }

        return null;
    }

    /**
     * Get instance of current helper
     * @return static
     * @throws InvalidConfigException
     * @throws \relynt\base\ApiResponseException
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (!isset(Yii::$app->api) or !isset(Yii::$app->api['version']) or Yii::$app->api['version'] != RelyntApi::API_VERSION_2) {
            throw new InvalidConfigException('Invalid api version');
        }

        if (self::$instance === null) {
            $config = [
                'api_domain' => Yii::$app->params['api_domain'],
            ];

            if (empty($config['api_domain'])) {
                throw new InvalidConfigException('Invalid API config');
            }

            $instance = new static($config);
            $instance->_api = new RelyntApi($instance->api_domain);
            $instance->_api->debug = false;
            $instance->_api->setVersion(RelyntApi::API_VERSION_2);

            $authType = RelyntApi::AUTH_TYPE_API_KEY;
            if (isset(Yii::$app->api['auth_type'])) {
                $authType = Yii::$app->api['auth_type'];
            }

            if (isset(Yii::$app->api) and isset(Yii::$app->api['disable_logout'])) {
                $instance->_disableLogout = Yii::$app->api['disable_logout'];
            }

            $authData = [];
            switch ($authType) {
                case RelyntApi::AUTH_TYPE_API_KEY:
                    $authData = [
                        'key' => isset(Yii::$app->params['api_key']) ? Yii::$app->params['api_key'] : null,
                        'secret' => isset(Yii::$app->params['api_secret']) ? Yii::$app->params['api_secret'] : null,
                    ];
                    break;
                // TODO think about auth with login and pass may be use Identity interface
//                case RelyntApi::AUTH_TYPE_ADMIN:
//                case RelyntApi::AUTH_TYPE_CUSTOMER:
//                    $authData = [
//                        'login' => isset(Yii::$app->params['api_login']) ? Yii::$app->params['api_login'] : null,
//                        'password' => isset(Yii::$app->params['api_password']) ? Yii::$app->params['api_password'] : null,
//                    ];
//                    break;
//                case RelyntApi::AUTH_TYPE_SESSION:
//                    $authData = [
//                        'session_id' => isset(Yii::$app->params['api_session_id']) ? Yii::$app->params['api_session_id'] : null,
//                    ];
//                    break;
            }

            if (!$instance->login($authType, $authData)) {
                $instance->checkResponseForError($instance->_api->response);
                // If the error is not received from the system
                throw new \Exception('Error: Authentication failed!');
            }

            self::$instance = $instance;
        }

        return self::$instance;
    }

    /**
     * Make GET request. Get records or record.
     * @param string $url API endpoint
     * @param null|int $id Record id
     * @return array
     * @throws InvalidConfigException
     */
    public static function get($url, $id = null)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make POST request. Create record.
     * @param string $url API endpoint
     * @param array $params Payload
     * @return array
     * @throws InvalidConfigException
     */
    public static function post($url, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post($url, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make PUT request. Update record by id.
     * @param string $url API endpoint
     * @param int $id Record id
     * @param array $params Payload
     * @return array
     * @throws InvalidConfigException
     */
    public static function put($url, $id, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_put($url, $id, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make DELETE request
     * @param string $url API endpoint
     * @param int $id Record id
     * @return array
     * @throws InvalidConfigException
     */
    public static function delete($url, $id)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_delete($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Search records by condition
     * @param string $url API endpoint
     * @param array $params Search condition
     * @return array
     * @throws InvalidConfigException
     */
    public static function search($url, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url . '?' . http_build_query($params));

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make OPTIONS request
     *
     * Get model properties.
     * Response example:
     *
     * ```php
     * [
     *      'attributes' => [
     *          [
     *              'name' => 'id',
     *              'title' => 'Id',
     *              'type' => 'integer',
     *              'required' => false,
     *          ],
     *      ],
     *      'additional_attributes' => [
     *          [
     *              'module' => 'locations',
     *              'name' => 'loc_ip',
     *              'title' => 'Location IP',
     *              'type' => 'relation',
     *              'required' => false,
     *              // ...
     *          ]
     *      ],
     * ]
     * ```
     * @param string $url API endpoint
     * @return array
     * @throws InvalidConfigException
     */
    public static function options($url)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_options($url);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Get amount of records
     *
     * Get amount of active customers:
     *
     * ```php
     * $result = $api->count('admin/customers/customer', [
     *      'main_attributes' => [
     *          'status' => 'active',
     *      ]
     * ]);
     * echo "\nAmount of active customers: " . var_export($result['response'], 1);
     * ```
     *
     * @param string $url API endpoint
     * @param array $condition Search condition
     * @return array
     * @throws InvalidConfigException
     */
    public static function count($url, $condition = [])
    {
        $api = self::getInstance()->_api;
        $condition = empty($condition) ? '' : '?' . http_build_query($condition);
        $result = $api->api_call_head($url . $condition);

        return [
            'result' => $result,
            'response' => isset($api->response_headers[self::HEADER_X_TOTAL_COUNT]) ? $api->response_headers[self::HEADER_X_TOTAL_COUNT] : null,
        ];
    }

    /**
     * Use this to upload file by API.
     *
     * @param $url
     * @param $id
     * @param array $params Array where key - property and value - CURLFile instance of file
     * @return array
     * @throws InvalidConfigException
     */
    public static function upload($url, $id, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post_file($url . '/' . $id . '--upload', $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make login. Generate JWT tokens pair
     * @param string $authType Possible values: `admin`, `customer`, `api_key`
     * @param array $data Auth data
     *
     * For login as admin
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     *      // 'code' => '', // If 2Fa enabled
     * ]
     * ```
     *
     * For login as customer
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     * ]
     * ```
     *
     * For login with using api key
     *
     * ```php
     * [
     *      'key' => '',
     *      'secret' => '',
     * ]
     * ```
     * @return bool
     * @throws \Exception
     */
    public function login($authType, $data)
    {
        $result = array_merge([
            'auth_type' => $authType
        ], $data);
        return $this->_api->login($result);
    }

    /**
     * Logout. Delete JWT token pair
     * @return array|bool
     */
    public function logout()
    {
        return $this->_api->logout();
    }

    /**
     * Get auth data
     * @return array
     */
    public function getAuthData()
    {
        return $this->_api->getAuthData();
    }

    /**
     * Set auth data. Set your auth data stored in external storage instead of login.
     * @param array $data
     * ```
     * [
     *      'access_token' => '',
     *      'access_token_expiration' => '',
     *      'refresh_token' => '',
     *      'refresh_token_expiration' => '',
     *      'permissions' => [],
     * ]
     * ```
     */
    public function setAuthData($data)
    {
        $this->_api->setAuthData($data);
    }

    /**
     * Method for logout before die
     *
     * If it is API v2 need logout before application die
     */
    public static function logoutBeforeDie()
    {
        try {
            if (empty(self::$instance)) {
                return;
            }

            if (self::$instance->_api->getVersion() != RelyntApi::API_VERSION_2) {
                return;
            }

            if (static::$instance->_disableLogout) {
                return;
            }

            $authData = self::$instance->_api->getAuthData();
            if (!empty($authData['refresh_token'])) {
                self::$instance->_api->logout();
            }
        } catch (\Exception $exception) {
            Yii::error('Error while logout Error:' . $exception->getMessage() . ' file: ' . $exception->getFile() . ':' . $exception->getLine());
        }
    }
}
