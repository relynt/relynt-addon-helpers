<?php
namespace relynt\v2\models\common;

use relynt\v2\base\BaseActiveApi;
use yii\validators\EmailValidator;

/**
 * Class BaseCustomerModel
 * Base model for same attributes in lead and customer
 * @package relynt\v2\models\common
 */
abstract class BaseCustomerModel extends BaseActiveApi
{
    public $id;
    public $login;
    public $partner_id;
    public $location_id;
    public $password;
    public $name;
    public $email;
    public $phone;
    public $street_1;
    public $zip_code;
    public $city;
    public $added_by = self::ADDED_BY_API;
    public $billing_type = self::BILLING_RECURRING;
    public $date_add;

    public $additional_attributes = [];

    //Billing types
    const BILLING_RECURRING = 'recurring';
    const BILLING_PREPAID = 'prepaid';
    const BILLING_PREPAID_MONTHLY = 'prepaid_monthly';

    //Added by
    const ADDED_BY_ADMIN = 'admin';
    const ADDED_BY_API = 'api';

    /**
     * Validate email
     * @param string $attribute
     * @param array $params
     */
    public function validateEmail($attribute, $params)
    {
        if (strpos($this->$attribute, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->$attribute));
        } else {
            $emails = [$this->$attribute];
        }

        $validator = new EmailValidator();

        foreach ($emails as $email) {
            if (!$validator->validate($email)) {
                $this->addError($attribute, "$email is not a valid email.");
            }
        }
    }

    /**
     * Get all emails
     * @return array
     */
    public function getEmailsArray()
    {
        if (empty($this->email)) {
            return [];
        }

        if (strpos($this->email, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->email));
        } else {
            $emails = [$this->email];
        }

        return $emails;
    }

    /**
     * Get all billing types
     * @return array
     */
    public static function getAllBillingTypes()
    {
        return [
            self::BILLING_PREPAID,
            self::BILLING_PREPAID_MONTHLY,
            self::BILLING_RECURRING
        ];
    }
}
