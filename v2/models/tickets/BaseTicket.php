<?php

namespace relynt\v2\models\tickets;

use relynt\v2\base\BaseActiveApi;

/**
 * Class BaseTicket
 * @package relynt\v2\models\tickets
 */
class BaseTicket extends BaseActiveApi
{
    public $id;
    public $customer_id;
    public $incoming_customer_id;
    public $hidden;
    public $assign_to;
    public $status_id;
    public $group_id;
    public $type_id;
    public $subject;
    public $priority;
    public $star;
    public $unread_by_customer;
    public $unread_by_admin;
    public $closed;
    public $created_at;
    public $updated_at;
    public $trash;
    public $lead_id;
    public $author;
    public $deleted;
    public $additional_attributes;

    protected static $apiUrl = 'admin/support/tickets';

    const PRIORITY_LOW = 'low';
    const PRIORITY_MEDIUM = 'medium';
    const PRIORITY_HIGH = 'high';
    const PRIORITY_URGENT = 'urgent';

    public function rules()
    {
        return [
            [['id', 'customer_id', 'incoming_customer_id', 'assign_to', 'status_id', 'group_id', 'star', 'unread_by_customer', 'unread_by_admin', 'closed', 'trash', 'deleted'], 'integer'],
            [['subject', 'priority'], 'string'],
            [['created_at', 'updated_at'], 'string'],
        ];
    }
}
