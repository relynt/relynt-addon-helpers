<?php

namespace relynt\v2\models\tickets;

use relynt\v2\base\BaseActiveApi;

/**
 * Class BaseTicketMessage
 * @package relynt\v2\models\tickets
 */
class BaseTicketMessage extends BaseActiveApi
{
    public $id;
    public $ticket_id;
    public $customer_id;
    public $incoming_customer_id;
    public $admin_id;
    public $api_id;
    public $date;
    public $time;
    public $message;
    public $files;
    public $is_merged;
    public $can_be_deleted;
    public $hide_for_customer;
    public $mail_to;
    public $mail_cc;
    public $mail_bcc;
    public $message_type;

    protected static $apiUrl = 'admin/support/ticket-messages';

    const MESSAGE_TYPE_MESSAGE = 'message';
    const MESSAGE_TYPE_NOTE = 'note';
    const MESSAGE_TYPE_CHANGE_TICKET = 'change_ticket';
}
