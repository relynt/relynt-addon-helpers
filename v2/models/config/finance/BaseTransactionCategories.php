<?php

namespace relynt\v2\models\config\finance;

use Exception;
use relynt\v2\base\BaseActiveApi;
use relynt\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\helpers\Inflector;

/**
 * Class BaseTransactionCategories
 * @method static mixed|null internetService($returnKey = 'id')
 * @method static mixed|null internetServiceDiscount($returnKey = 'id')
 * @method static mixed|null internetTopUp($returnKey = 'id')
 * @method static mixed|null voiceService($returnKey = 'id')
 * @method static mixed|null voiceServiceDiscount($returnKey = 'id')
 * @method static mixed|null voiceServiceCalls($returnKey = 'id')
 * @method static mixed|null voiceServiceMessages($returnKey = 'id')
 * @method static mixed|null voiceServiceData($returnKey = 'id')
 * @method static mixed|null customService($returnKey = 'id')
 * @method static mixed|null customServiceDiscount($returnKey = 'id')
 * @method static mixed|null oneTimeService($returnKey = 'id')
 * @method static mixed|null bundleService($returnKey = 'id')
 * @method static mixed|null bundleServiceDiscount($returnKey = 'id')
 * @method static mixed|null bundleActivationFee($returnKey = 'id')
 * @method static mixed|null bundleCancellationFee($returnKey = 'id')
 * @method static mixed|null inventorySell($returnKey = 'id')
 * @method static mixed|null invoiceItem($returnKey = 'id')
 * @package relynt\v2\models\config\finance
 */
class BaseTransactionCategories extends BaseActiveApi
{
    /**
     * @var array|null
     */
    protected static $_types = null;

    /**
     * @var string
     */
    public static $apiUrl = 'admin/config/transaction-categories';

    /**
     * @return array|BaseTransactionCategories[]|null
     * @throws InvalidConfigException|Exception
     */
    public static function getListAll()
    {
        self::getData();
        return self::$_types;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed|null
     * @throws InvalidConfigException
     */
    public static function __callStatic($name, $arguments)
    {
        self::getData();
        $returnKey = (empty($arguments)) ? 'id' : reset($arguments);
        return (isset(self::$_types['transaction_type_' . Inflector::underscore($name)][$returnKey])) ? self::$_types['transaction_type_' . Inflector::underscore($name)][$returnKey] : null;
    }

    /**
     * @return array
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected static function getData()
    {
        if (empty(self::$_types)) {
            $result = ApiHelper::getInstance()->get((new self())->getApiUrl());
            if (!$result['result']) {
                throw new Exception('Invalid api call');
            }

            self::$_types = reset($result['response']);
        }

        return self::$_types;
    }
}
