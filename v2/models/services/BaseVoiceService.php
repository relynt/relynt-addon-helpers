<?php

namespace relynt\v2\models\services;

/**
 * Class BaseVoiceService
 * @package relynt\models\services
 */
class BaseVoiceService extends BaseService
{
    public $voice_device_id;
    public $phone;

    public function getServiceApiUrl()
    {
        return 'voice-services';
    }
}
