<?php

namespace relynt\v2\models\services;

use relynt\v2\base\BaseActiveApi;
use relynt\v2\helpers\ApiHelper;

/**
 * Class BaseService
 * @package relynt\models\services
 */
abstract class BaseService extends BaseActiveApi
{
    public $id;
    public $parent_id;
    public $customer_id = 0;
    public $tariff_id;
    public $status = self::STATUS_ACTIVE;
    public $status_new;
    public $description;
    public $quantity = 1;
    public $unit;
    public $unit_price;
    public $start_date;
    public $end_date;
    public $discount;
    public $discount_percent;
    public $discount_start_date;
    public $discount_end_date;
    public $discount_text;
    public $bundle_service_id;

    public $additional_attributes = [];

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_STOPPED = 'stopped';
    const STATUS_PENDING = 'pending';

    public static $apiUrl = 'admin/customers/customer';

    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = self::$apiUrl . '/' . $this->customer_id . '/' . $this->getServiceApiUrl();

        // Set id
        if ($id !== null) {
            $result .= '--' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * Method is overwritten because services has own logic (SPL-1117)
     * @param $id
     * @return null|BaseActiveApi
     */
    public function findById($id)
    {
        return parent::findOne([
            'id' => $id
        ]);
    }

    abstract protected function getServiceApiUrl();

    /**
     * Delete service. Method is overwritten because in services we use custom URLs
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function delete()
    {
        $result = ApiHelper::getInstance()->delete($this->getApiUrl($this->id), null);
        return $result['result'];
    }
}
