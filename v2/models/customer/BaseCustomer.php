<?php

namespace relynt\v2\models\customer;

use relynt\v2\models\common\BaseCustomerModel;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class BaseCustomer
 * @package relynt\models\customer
 */
class BaseCustomer extends BaseCustomerModel implements IdentityInterface
{
    public $category = self::CATEGORY_PERSON;
    public $status;
    public $last_online;
    public $last_update;

    public static $apiUrl = 'admin/customers/customer';

    // Categories
    const CATEGORY_PERSON = 'person';
    const CATEGORY_COMPANY = 'company';

    // Statuses
    const STATUS_NEW = 'new';
    const STATUS_ACTIVE = 'active';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_DOESNT_USE_SERVICES = 'disabled';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['name', 'partner_id', 'location_id', 'category'], 'required'],
            ['email', 'validateEmail'],
            ['added_by', 'in', 'range' => [static::ADDED_BY_ADMIN, static::ADDED_BY_API]],
            ['billing_type', 'in', 'range' => static::getAllBillingTypes()],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return (new static())->findById($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }
}
