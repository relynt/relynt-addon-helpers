<?php

namespace relynt\v2\models\finance;

use relynt\v2\base\BaseActiveApi;
use relynt\v2\models\finance\item\BaseItem;
use relynt\v2\models\finance\item\ItemsObject;
use yii\helpers\ArrayHelper;

/**
 * Class BaseFinanceClass
 *
 * adding property and methods for use Items in the Finance classes.
 *
 * usage:
 *
 * create item
 *
 * ```php
 * $newItem = ['id'=>1,'description'=>'Desc'...]
 * ```
 *
 * or
 *
 * ```php
 * $newItem = new Item(['id'=>1,'description'=>'Desc'...]);
 * ```
 * or
 *
 * ```php
 * $newItem = new Item();
 * $newItem->id = 1;
 * $newItem->description = 'Desc';
 * $newItem->...
 * ```
 *
 * Add Item
 *
 * ```php
 * $model->items[] = $newItem;
 * ```
 *
 * Change Item
 *
 * ```php
 * $item = $model->items[0];
 * $item->description = 'New description'
 * ```
 *
 * @property BaseItem[]|ItemsObject $items
 * */
class BaseFinanceClass extends BaseActiveApi
{
    /**
     * @var ItemsObject|BaseItem[]
     */
    protected $_items;

    /**
     * @return BaseItem[]
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @param array|BaseItem[] $items
     */
    public function setItems(array $items)
    {
        foreach ($items as $item) {
            $this->items[] = $item;
        }
    }

    /**
     * @param BaseFinanceClass $model
     * @inheritdoc
     */
    public static function populate($model, $data)
    {
        //Populate Items to model
        $items = null;
        if (array_key_exists('items', $data)) {
            $items = $data['items'];
            unset($data['items']);
        }
        parent::populate($model, $data);
        $model->items = $items;
    }

    /**
     * @inheritdoc
     * and validate items before save
     */
    public function save($runValidation = true)
    {
        if ($runValidation) {
            // Validate Items Before save
            foreach ($this->items as $item) {
                if (!$item->validate()) {
                    $this->addError('items', json_encode($item->getErrors()));
                }
            }
            if (!empty($this->errors)) {
                return false;
            }
        }
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     *
     * Add Items to attributesForSend
     *
     * @see BaseActiveApi::getAttributesForSend()
     */
    public function getAttributesForSend()
    {
        //Add Items to attributesForSend
        $attributesForSend = parent::getAttributesForSend();
        $attributesForSend['items'] = [];
        foreach ($this->items as $item) {
            $attributesForSend['items'][] = ArrayHelper::toArray($item);
        }
        return $attributesForSend;
    }

    /**
     * @param integer $number
     * @return $this|null
     */
    public static function findByNumber($number)
    {
        $invoice = new static();
        return $invoice->findOne(['number' => $number]);
    }

    /**
     * Update Invoice and reload attributes.
     * @inheritdoc
     */
    public function update($runValidation = true)
    {
        $this->reloadAttributesAfterSave = true;
        return parent::update($runValidation);
    }
}
