<?php

namespace relynt\v2\models\finance;

use relynt\v2\models\finance\item\ItemsObject;

/**
 * Class BaseInvoice
 * @package relynt\models\finance
 */
class BaseInvoice extends BaseFinanceClass
{
    public $id;
    public $customer_id;
    public $date_created;
    public $date_payment;
    public $date_till;
    public $date_updated;
    public $disable_cache;
    public $memo;
    public $note;
    public $number;
    public $payment_id;
    public $status;
    public $total;
    public $use_transactions;
    public $payd_from_deposit;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/invoices';

    const STATUS_PAID = 'paid';
    const STATUS_NOT_PAID = 'not_paid';
    const STATUS_DELETED = 'deleted';
    const STATUS_PENDING = 'pending';

    /**
     * Set items object and items model
     */
    public function init()
    {
        $this->_items = new ItemsObject();
        $this->_items->model = BaseInvoice::className();
        parent::init();
    }
}
