<?php

namespace relynt\v2\models\finance\item;

/**
 * Class BaseInvoiceItem
 * @package relynt\models\finance\item
 */
class BaseInvoiceItem extends BaseItem
{
    /** @var  int */
    public $transaction_id;
    /** @var int */
    public $categoryIdForTransaction;
}
