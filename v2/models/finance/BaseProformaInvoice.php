<?php

namespace relynt\v2\models\finance;

use relynt\v2\models\finance\item\ItemsObject;

/**
 * Class BaseProformaInvoice
 * @package relynt\models\finance
 */
class BaseProformaInvoice extends BaseFinanceClass
{
    public $id;
    public $customer_id;
    public $date_created;
    public $real_create_datetime;
    public $date_updated;
    public $date_payment;
    public $date_till;
    public $number;
    public $total;
    public $payment_id;
    public $status;
    public $is_sent;
    public $note;
    public $memo;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/requests';

    const STATUS_PAID = 'paid';
    const STATUS_NOT_PAID = 'not_paid';
    const STATUS_PENDING = 'pending';

    /**
     *  Set items object
     */
    public function init()
    {
        $this->_items = new ItemsObject();
        parent::init();
    }
}
