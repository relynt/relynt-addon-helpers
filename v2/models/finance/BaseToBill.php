<?php

namespace relynt\v2\models\finance;

use relynt\v2\helpers\ApiHelper;
use yii\base\BaseObject;

/**
 * Class BaseToBill
 * @package relynt\models\finance
 */
class BaseToBill extends BaseObject
{
    public $customer_id;
    public $action;

    public $toBillDate;
    public $transactionDate;
    public $period;

    public $result;
    public $invoice;

    const ACTION_PREVIEW = 'preview';
    const ACTION_GENERATE = 'generate';

    public static $apiUrl = 'admin/finance/to-bill';

    protected function getApiUrl()
    {
        return static::$apiUrl . '/' . $this->customer_id . '--' . $this->action;
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function call()
    {
        $attributes = [
            'toBillDate' => $this->toBillDate,
            'transactionDate' => $this->transactionDate,
            'period' => $this->period,
        ];

        $result = ApiHelper::getInstance()->post($this->getApiUrl(), $attributes);

        if ($result['result'] == true) {
            $this->result = $result['response']['result'];
            $this->invoice = $result['response']['invoice'];
        }

        return $result['result'];
    }
}
