<?php

namespace relynt\v2\models\inventory;

use relynt\v2\base\BaseActiveApi;

/**
 * Class BaseItem
 * @package relynt\v2\models\inventory
 */
class BaseItem extends BaseActiveApi
{
    public $id;
    public $supplier_invoices_item_id;
    public $barcode;
    public $status;
    public $mark;
    public $customer_id;
    public $admin_id;
    public $service_id;
    public $invoice_id;
    public $product_id;
    public $notes;
    public $filename_original;
    public $filename;
    public $stock_id;
    public $file_link;

    public $additional_attributes = [];

    const STATUS_STOCK = 'stock';
    const STATUS_INTERNAL_USAGE = 'internal_usage';
    const STATUS_SOLD = 'sold';
    const STATUS_RENT = 'rent';
    const STATUS_RETURNED = 'returned';
    const STATUS_ASSIGNED = 'assigned';

    const MARK_NEW = 'new';
    const MARK_USED = 'used';
    const MARK_BROKEN = 'broken';

    public static $apiUrl = 'admin/inventory/items';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['id', 'supplier_invoices_item_id', 'customer_id', 'admin_id', 'service_id', 'invoice_id', 'product_id', 'stock_id'], 'integer'],
            [['barcode', 'status', 'mark', 'notes', 'filename_original', 'filename', 'file_link'], 'string'],
            ['status', 'in', 'range' => $this->getStatusArray()],
            ['mark', 'in', 'range' => $this->getMarksArray()],
        ];
    }

    /**
     * @return array of item statuses
     */
    public function getStatusArray()
    {
        return [
            static::STATUS_STOCK,
            static::STATUS_INTERNAL_USAGE,
            static::STATUS_SOLD,
            static::STATUS_RENT,
            static::STATUS_RETURNED,
            static::STATUS_ASSIGNED,
        ];
    }

    /**
     * @return array of item marks
     */
    public function getMarksArray()
    {
        return [
            static::MARK_BROKEN,
            static::MARK_NEW,
            static::MARK_USED,
        ];
    }
}
