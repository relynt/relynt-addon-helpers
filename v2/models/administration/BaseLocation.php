<?php

namespace relynt\v2\models\administration;

use relynt\v2\base\BaseActiveApi;

/**
 * Class BaseLocation
 * @package relynt\models\administration
 */
class BaseLocation extends BaseActiveApi
{
    public $id;
    public $name;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/locations';


    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
