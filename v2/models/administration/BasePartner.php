<?php

namespace relynt\v2\models\administration;

use relynt\v2\base\BaseActiveApi;

/**
 * Class BasePartner
 * @package relynt\models\administration
 */
class BasePartner extends BaseActiveApi
{
    public $id;
    public $name;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/partners';

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
