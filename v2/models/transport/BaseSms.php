<?php

namespace relynt\v2\models\transport;

use relynt\helpers\ArrayHelper;

/**
 * Class BaseSms
 * @package relynt\v2\models\transport
 */
class BaseSms extends BaseMessage
{
    protected static $apiUrl = 'admin/config/sms';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['recipient'], 'string', 'max' => 64],
        ], parent::rules());
    }
}
