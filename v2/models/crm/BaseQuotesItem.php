<?php

namespace relynt\v2\models\crm;

use yii\base\Model;

/**
 * Class BaseQuoteItem
 * @package relynt\v2\models\crm
 */
class BaseQuotesItem extends Model
{
    /** @var  int */
    public $id;
    /** @var  int */
    public $pos;
    /** @var  int */
    public $tax_amount;

    /** @var  string */
    public $description;

    /** @var  int */
    public $quantity;

    /** @var  number */
    public $unit;

    /** @var  double */
    public $price;

    /** @var  double */
    public $tax;

    /**
     * Set here tariff in format {tariff_category}--{tariff_id}
     * Example internet--1
     * @var string $tariff
     */
    public $tariff;

    // tariffs categories for items
    const TARIFF_INTERNET = 'internet';
    const TARIFF_VOICE = 'voice';
    const TARIFF_CUSTOM = 'custom';
    const TARIFF_BUNDLE = 'bundle';
    const TARIFF_ONE_TIME = 'one_time';

    /**
     * BaseQuotesItem constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        foreach ($config as $key => $prop) {
            if (!property_exists($this, $key)) {
                unset($config[$key]);
            }
        }
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['description', 'quantity', 'price'], 'required'],
            [['price', 'tax'], 'double'],
            ['quantity', 'default', 'value' => 1],
            ['quantity', 'integer', 'min' => 1,],
        ];
    }
}
