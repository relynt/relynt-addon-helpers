<?php

namespace relynt\v2\models\crm;

use relynt\v2\models\finance\BaseFinanceClass;
use relynt\v2\models\finance\item\ItemsObject;

/**
 * Class BaseQuote
 * @package relynt\v2\models\crm
 */
class BaseQuote extends BaseFinanceClass
{
    public $id;
    public $customer_id;
    public $number;
    public $date_created;
    public $real_create_datetime;
    public $date_updated;
    // date when quote was accepted or denied
    public $date_of_decision;
    public $date_till;
    public $total;
    public $invoice_id;
    public $request_id;
    public $status;
    public $is_sent;
    public $note;
    public $memo;
    public $is_customers = 0;
    public $deal_id;
    public $added_by;
    public $added_by_id;

    // quotes statuses
    const STATUS_NEW = 'new';
    const STATUS_SENT = 'sent';
    const STATUS_ON_REVIEW = 'on_review';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_DENIED = 'denied';

    const ADDED_BY_SYSTEM = 'system';
    const ADDED_BY_API = 'api';
    const ADDED_BY_ADMIN = 'admin';
    const ADDED_BY_WIDGET = 'widget';

    public static $apiUrl = 'admin/crm/quotes';

    /**
     * Set items object and items model
     */
    public function init()
    {
        $this->_items = new ItemsObject();
        $this->_items->model = BaseQuote::className();
        parent::init();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'status'], 'required'],
            ['status', 'in', 'range' => static::getQuotesStatusArray()],
            ['added_by', 'in', 'range' => static::getAddedByArray()],
        ];
    }

    /**
     * @return array
     */
    public static function getQuotesStatusArray()
    {
        return [
            static::STATUS_NEW,
            static::STATUS_SENT,
            static::STATUS_ON_REVIEW,
            static::STATUS_ACCEPTED,
            static::STATUS_DENIED,
        ];
    }

    /**
     * @return array
     */
    public static function getAddedByArray()
    {
        return [
            static::ADDED_BY_SYSTEM,
            static::ADDED_BY_API,
            static::ADDED_BY_ADMIN,
            static::ADDED_BY_WIDGET,
        ];
    }
}
