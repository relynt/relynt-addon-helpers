<?php

namespace relynt\v2\models\crm;

use relynt\v2\base\BaseActiveApi;
use relynt\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\NotSupportedException;

/**
 * Class BaseLeadsNote
 * @package relynt\v2\models\crm
 */
class BaseLeadsNote extends BaseActiveApi
{
    public $id;
    public $customer_id;
    public $datetime;
    public $administrator_id;
    public $type;
    public $class;
    public $title;
    public $name;
    public $comment;
    public $values;

    const TYPE_COMMENT = 'comment';
    const TYPE_CUSTOMER = 'customer';
    const TYPE_FINANCE = 'finance';
    const TYPE_SERVICE = 'service';
    const TYPE_MESSAGE = 'message';
    const TYPE_CALL = 'call';

    public static $apiUrl = 'admin/crm/leads-notes';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'comment'], 'required'],
            ['type', 'in', 'range' => static::getTypesArray()],
            ['comment', 'validateComment'],
        ];
    }

    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            static::TYPE_COMMENT,
            static::TYPE_CUSTOMER,
            static::TYPE_FINANCE,
            static::TYPE_SERVICE,
            static::TYPE_MESSAGE,
            static::TYPE_CALL,
        ];
    }

    /**
     * Validate comment
     * @param string $attribute
     * @param array $params
     * @return bool
     */
    public function validateComment($attribute, $params)
    {
        if (in_array(trim($this->{$attribute}), ['', '<br>'])) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function update($runValidation = true)
    {
        throw new NotSupportedException(\Yii::t('app', 'Update action is not supported'));
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function delete()
    {
        if (empty($this->customer_id)) {
            throw new InvalidParamException(\Yii::t('app', 'You must set customer_id value'));
        }

        if ($this->id === null) {
            return false;
        }
        $id = $this->customer_id . '--' . $this->id;

        $result = ApiHelper::getInstance()->delete($this->getApiUrl(), $id);
        if ($result['result'] === true) {
            $this->cleanOldAttributes();
        }
        return $result['result'];
    }
}
