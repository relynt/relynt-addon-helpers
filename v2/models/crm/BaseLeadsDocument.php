<?php

namespace relynt\v2\models\crm;

use relynt\v2\models\customer\BaseCustomerDocument;

/**
 * Class BaseLeadsDocument
 * @package relynt\v2\models\customer
 */
class BaseLeadsDocument extends BaseCustomerDocument
{
    public static $apiUrl = 'admin/crm/leads-documents';
}
