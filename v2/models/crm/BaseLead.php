<?php

namespace relynt\v2\models\crm;

use relynt\v2\models\common\BaseCustomerModel;

/**
 * Class Leads
 * @package relynt\v2\models\crm
 */
class BaseLead extends BaseCustomerModel
{
    public static $apiUrl = 'admin/crm/leads';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['name', 'partner_id', 'location_id'], 'required'],
            ['email', 'validateEmail'],
            ['added_by', 'in', 'range' => [static::ADDED_BY_ADMIN, static::ADDED_BY_API]],
            ['billing_type', 'in', 'range' => static::getAllBillingTypes()],
        ];
    }
}
