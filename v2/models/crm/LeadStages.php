<?php

namespace relynt\v2\models\crm;

use relynt\v2\base\BaseActiveApi;

/**
 * Class LeadStages
 * @package relynt\v2\models\crm
 * @deprecated
 */
class LeadStages extends BaseActiveApi
{
    public $id;
    public $name;
    public $is_base;

    public static $apiUrl = 'admin/config/crm-lead-stages';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string']
        ];
    }
}
