<?php

namespace relynt\v2\models\crm;

use relynt\v2\base\BaseActiveApi;

/**
 * Class Pipeline
 * @package relynt\v2\models\crm
 */
class Pipeline extends BaseActiveApi
{
    public $id;
    public $name;
    public $is_base;

    public static $apiUrl = 'admin/config/crm-pipeline';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string']
        ];
    }
}
