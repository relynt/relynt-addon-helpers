<?php

namespace relynt\v2\models\crm;

use relynt\v2\base\BaseActiveApi;
use yii\base\InvalidCallException;
use yii\base\NotSupportedException;

/**
 * Class BaseLeadsInfo
 * @package relynt\v2\models\crm
 */
class BaseLeadsInfo extends BaseActiveApi
{
    public $customer_id;
    public $score;
    public $last_contacted;
    /**
     * ID of crm status from relynt config
     * @var integer $crm_status
     */
    public $crm_status;
    /**
     * ID of admin
     * @var integer $owner
     */
    public $owner;
    public $source;

    public static $apiUrl = 'admin/crm/leads-info';

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return 'customer_id';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
        ];
    }

    /**
     * @return bool|void
     * @throws NotSupportedException
     */
    public function delete()
    {
        throw new NotSupportedException(\Yii::t('app', 'Delete action is not supported'));
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function create($runValidation = true)
    {
        throw new NotSupportedException(\Yii::t('app', 'Create action is not supported'));
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function findAll(array $mainAttributes, array $additionalAttributes = [], array $order = [], $limit = null, $offset = null)
    {
        throw new NotSupportedException(\Yii::t('app', 'Search action is not supported'));
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function findOne(array $mainAttributes, array $additionalAttributes = [], array $order = [])
    {
        throw new NotSupportedException(\Yii::t('app', 'Search action is not supported'));
    }
}
