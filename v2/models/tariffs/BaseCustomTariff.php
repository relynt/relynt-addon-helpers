<?php

namespace relynt\v2\models\tariffs;

/**
 * Class BaseCustomTariff
 * @package relynt\models\tariffs
 */
class BaseCustomTariff extends BaseTariff
{
    public static $apiUrl = 'admin/tariffs/custom';
}
