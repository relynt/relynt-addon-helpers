<?php

namespace relynt\base;

use relynt\helpers\ApiHelper;
use yii\base\InvalidCallException;

/**
 * Class BasePaymentAccount.
 */
class BasePaymentAccount extends BaseApiModel
{
    public $account_id;
    public $customer_id;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;
    public $field_6;
    public $field_7;
    public $field_8;
    public $field_9;
    public $field_10;

    const API_URL = 'admin/customers/customer-payment-accounts';

    /**
     * @param int $customerId
     * @param int $accountId
     * @return string
     */
    protected static function getApiUrl($customerId, $accountId)
    {
        return static::API_URL . '/' . $customerId . '--' . $accountId;
    }

    /**
     * @param int $customerId
     * @param int $accountId
     * @return null|static
     * @throws \yii\base\InvalidConfigException
     */
    public static function getCustomerData($customerId, $accountId)
    {
        $result = ApiHelper::getInstance()->get(static::getApiUrl($customerId, $accountId));

        if ($result['result'] == false or empty($result['response'])) {
            return null;
        }

        $model = new static();
        static::populate($model, $result['response']);
        return $model;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function update()
    {
        $params = [
            'account_id' => $this->account_id,
            'customer_id' => $this->customer_id,
            'field_1' => $this->field_1,
            'field_2' => $this->field_2,
            'field_3' => $this->field_3,
            'field_4' => $this->field_4,
            'field_5' => $this->field_5,
            'field_6' => $this->field_6,
            'field_7' => $this->field_7,
            'field_8' => $this->field_8,
            'field_9' => $this->field_9,
            'field_10' => $this->field_10
        ];

        $result = ApiHelper::getInstance()->put(static::getApiUrl($this->customer_id, $this->account_id), null, $params);
        if ($result['result'] == false) {
            throw new InvalidCallException('Error in API Call!');
        }

        return true;
    }
}
