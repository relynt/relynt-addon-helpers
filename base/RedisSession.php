<?php

namespace relynt\base;

use relynt\helpers\ConfigHelper;
use yii\redis\Session;

/**
 * Class RedisSession provide functionality for working with Relynt session that stored in Redis
 * @package relynt\base
 */
class RedisSession extends Session
{
    /** @var string Session key prefix */
    public $keyPrefix = 'php_session:';

    /** @var string Relynt prefix for keys in redis */
    public $relyntRedisPrefix;

    /** Default prefix for all relynt keys stored in redis */
    const DEFAULT_RELYNT_REDIS_KEY_PREFIX = 'rel:';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $redisConfig = ConfigHelper::getRedisConfig();
        $redisConfig = isset($redisConfig['server']) ? $redisConfig['server'] : $redisConfig;
        $this->relyntRedisPrefix = isset($redisConfig['prefix']) ? $redisConfig['prefix'] : self::DEFAULT_RELYNT_REDIS_KEY_PREFIX;

        ini_set('session.serialize_handler', 'msgpack');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function calculateKey($id)
    {
        return $this->relyntRedisPrefix . $this->keyPrefix . $id;
    }

    /**
     * @inheritdoc
     */
    public function readSession($id)
    {
        $sessionData = parent::readSession($id);
        if (empty($sessionData)) {
            return '';
        }

        $sessionData = json_decode($sessionData, true);
        return \msgpack_pack($sessionData);
    }

    /**
     * @inheritdoc
     */
    public function writeSession($id, $data)
    {
        $data = json_encode($_SESSION);
        return parent::writeSession($id, $data);
    }

    /**
     * @param string $key
     * @return string
     */
    public function getPrimaryFromKey($key)
    {
        $parts = explode(':', $key);
        if (!empty($parts)) {
            return end($parts);
        }

        return null;
    }

    /**
     * @return string[]
     */
    public function getAllOnlineAdmins()
    {
        $pattern = $this->relyntRedisPrefix . 'record:online_admin:data';
        $data = $this->redis->hgetall($pattern);
        $sessions = [];
        $sessionKey = null;
        foreach ($data as $key => $session) {
            if ($key === 0 || ($key % 2) == 0) {
                $sessionKey = $session;
                continue;
            }
            $sessions[$sessionKey] = $session;
        }
        return $sessions;
    }

    /**
     * Get active sessions for online admins by admin ids
     * @param int[] $adminIds Admins ids
     * @return array
     */
    public function getSessionsIdsForOnlineAdmins($adminIds)
    {

        $allSessions = $this->getAllOnlineAdmins();
        $sessions = [];
        foreach ($allSessions as $key => $session) {
            $session = json_decode($session, 1);
            if (isset($session['admin_id']) && in_array($session['admin_id'], $adminIds) && $key) {
                $sessions[] = $key;
            }
        }
        return $sessions;
    }
}
