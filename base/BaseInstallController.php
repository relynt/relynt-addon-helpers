<?php

namespace relynt\base;

use relynt\helpers\ConfigHelper;
use relynt\helpers\IPHelper;
use relynt\models\console_api\config\integration\ConsoleModuleConfig;
use yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This class could help for create install controllers in add-ons.
 * For use need extend this class in your InstallController class and redefine needs methods.
 * You need redefine base methods (See abstract methods) in your install controller class.
 * This class required Relynt with version >= 2.0.
 * @package relynt\base
 */
abstract class BaseInstallController extends Controller
{
    /**
     * Minimum Relynt version needs for this add-on. If empty then version will not check.
     * @var string
     */
    public static $minimumRelyntVersion = '2.0';

    /**
     * Full path to Relynt version file
     * @deprecated moved to ConfigHelper::getRelyntVersionPath
     */
    const VERSION_FILE_PATH = '/var/www/relynt/version';

    /** Name of add-on encryption key field in params file */
    const ENCRYPTION_KEY_FIELD = 'add_on_encryption_key';

    /** Name of cookie validation key field in params file */
    const COOKIE_VALIDATION_KEY_FIELD = 'cookieValidationKey';

    /** Name of api domain field in params file */
    const API_DOMAIN_FIELD = 'api_domain';

    /** Name of apikey key field in params file */
    const API_KEY_FIELD = 'api_key';

    /** Name of apikey secret field in params file */
    const API_SECRET_FIELD = 'api_secret';

    /** Default rule for apikey permission rule */
    const DEFAULT_PERMISSION_RULE = 'allow';

    const INSTALL_PROCESS = 'install';
    const UPDATE_PROCESS = 'update';

    // Hanldlers
    const FINANCE_BANK_STATEMENS_HANDLERS = 'bank-statements';
    const FINANCE_CHARGE_HANDLERS = 'charge';
    const FINANCE_EXPORT_HANDLERS = 'export';

    /**
     * @var string $env if you want to install dev environment it should be `dev`. Default `prod'.
     * Usage: ./yii install -env=dev
     */
    public $env;


    const MODULE_STATUS_ENABLED = 1;
    const MODULE_STATUS_DISABLED = 0;

    /**
     * By default module is disabled, if you want to enable module after install set $module_status = self::MODULE_STATUS_ENABLED
     * @var string $module_status '0' '1' default '0'
     */
    public $module_status;

    /**
     * Property to check the type of process (install or update process)
     * @var string
     */
    protected $process;

    /**
     * Set options
     *
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return ['env'];
    }

    /**
     * Set alias for options
     *
     * @return array
     */
    public function optionAliases()
    {
        return ['env' => 'env', 'e' => 'env'];
    }

    /**
     * Full install add-on to Relynt.
     * Install add-on in step by step:
     * - Check Relynt version.
     * - Create Relynt apikey.
     * - Set apikey permissions.
     * - Create Relynt module.
     * - Create entry points.
     * - Create additional fields.
     * - Create hooks with events.
     * - Create and prepare params.php file with default settings and extended params.example.php file.
     * - Chmod files and directories.
     * - Restart server process (nginx or apache).
     * - Run migration if directory migrations is exist.
     * @throws yii\base\Exception
     */
    public function actionIndex()
    {
        echo "Start install..\n";

        if ($this->env == 'dev') {
            $this->stdout('Start to create dev environment...' . PHP_EOL);
        }
        if ($this->createEnvironment($this->env) && $this->env == 'dev') {
            $this->stdout('Dev environment has been successfully created!' . PHP_EOL);
        }

        $apiKeyId = $this->createApiKey();
        if (!$apiKeyId) {
            exit('Create API key failed!' . "\n");
        }

        // add it here because maybe we will need to add permissions for the module
        if (!$this->createModules()) {
            exit('Create modules has failed!' . "\n");
        }

        $this->setApiPermissions($apiKeyId);

        $apiData = static::getApiKeyAndSecretById($apiKeyId);
        if ($apiData === false) {
            exit("Get API key anf secret failed!\n");
        }

        if (!$this->createModule()) {
            exit('Error adding ' . $this->getModuleName());
        }

        if (!$this->createPaymentAccounts()) {
            exit('Create payment accounts has failed!' . "\n");
        }

        if (!$this->createEntryPoints()) {
            exit('Add entry points failed!' . "\n");
        }

        if (!$this->createAdditionalFields()) {
            exit('Create additional fields failed!' . "\n");
        }

        if (!$this->createHooks()) {
            exit('Create hooks failed!' . "\n");
        }

        if (!$this->removeFinancialHandlers()) {
            exit('Remove financial handlers has failed!' . "\n");
        }

        if (!$this->copyFinancialHandlers()) {
            exit('Copying financial handlers has failed!' . "\n");
        }

        $this->prepareParamsFile($apiData['key'], $apiData['secret']);

        ConfigHelper::reloadConfig();

        if (is_dir(Yii::$app->getBasePath() . '/migrations')) {
            Yii::$app->runAction('migrate/up', [
                'interactive' => '0'
            ]);
        }

        // Set permissions
        static::chmodFiles($this->getFilesPermission());

        // Reload nginx
        if (file_exists('/etc/init.d/nginx')) {
            exec('/etc/init.d/nginx restart  > /dev/null 2>&1 3>/dev/null');
        }

        // Reload apache
        if (file_exists('/etc/init.d/apache2')) {
            exec('/etc/init.d/apache2 restart  > /dev/null 2>&1 3>/dev/null');
        }

        echo "Done!\n";
    }

    /**
     * If environment is `dev` - generate `dev.php` file in `config` folder with necessary params.
     * If environment is `prod` - delete `dev.php` file if exists.
     * Exit from action if something went wrong.
     *
     * @param string $env if `dev` - dev env., in other cases - `prod`.
     * @return bool `true` - if everything is OK.
     */
    protected function createEnvironment($env)
    {
        $filePath = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'dev.php';

        if ($env != 'dev') {
            if (file_exists($filePath) && unlink($filePath) === false) {
                $this->stderr('Error was occurred trying to delete ' . $filePath . ' file.' . PHP_EOL);
            }
            return true;
        }

        $file = fopen($filePath, 'w');
        if ($file === false) {
            $this->stderr('Error was occurred trying to create ' . $filePath . ' for dev environment.' . PHP_EOL);
            exit(ExitCode::OSFILE);
        }

        $content = "<?php" . PHP_EOL;
        $content .= "defined('YII_DEBUG') or define('YII_DEBUG', true);" . PHP_EOL;
        $content .= "defined('YII_ENV') or define('YII_ENV', 'dev');" . PHP_EOL;
        if (fwrite($file, $content) === false) {
            $this->stderr('Error was occurred trying to write content in  ' . $filePath . ' for dev environment.' . PHP_EOL);
            exit(ExitCode::OSFILE);
        }

        fclose($file);

        return true;
    }

    /**
     * Generate and save encryption key to current add-on. Return null if error.
     * @return null|string - Encryption key.
     * @throws yii\base\Exception
     */
    public function actionGenerateKey()
    {
        $paramsFile = static::getParamsFilePath();
        if (!file_exists($paramsFile)) {
            echo 'params.php is not exist! Please run `./yii install`' . "\n";
            return null;
        }

        $params = require($paramsFile);
        if (isset($params[static::ENCRYPTION_KEY_FIELD]) and !empty($params[static::ENCRYPTION_KEY_FIELD])) {
            echo 'Encryption key already set!' . "\n";
            return null;
        }

        $encryptionKey = static::generateEncryptionKey();
        $comment = 'Required param for encrypting add-on settings with type \'encrypted\', PLEASE DON\'T DELETE';

        $insertResult = static::insertParamToFile($paramsFile, static::ENCRYPTION_KEY_FIELD, $encryptionKey, $comment);
        if ($insertResult === false) {
            echo 'Error writing encryption key to params.php!' . "\n";
            return null;
        }

        echo 'Encryption key was generated!' . "\n";

        // Reload add-on configs
        exec(static::getRelyntDir() . 'system/setup/setup reload-configs');

        return $encryptionKey;
    }

    /**
     * Get add-on title.
     * @return string
     */
    abstract public function getAddOnTitle();

    /**
     * Get name of add-on module
     * @return string
     */
    abstract public function getModuleName();

    /**
     * Get api permissions for add-on.
     * Example return data:
     *
     *```php
     *[
     *  [
     *      'controller' => 'api\admin\customers\Customer',
     *      'actions' => ['index', 'add', 'delete'],
     *      'rule' => 'deny'
     *  ]
     *]
     * ```
     * Where 'api\admin\customers\Customer' is controller name and 'index' is action of controller.
     * @return array
     */
    abstract public function getApiPermissions();

    /**
     * Get add-on entry points.
     * @return array
     */
    public function getEntryPoints()
    {
        return [];
    }

    /**
     * Get files permissions.
     * Use this method for set custom permissions for your files or directories.
     * @return array
     */
    public function getFilesPermission()
    {
        return [
            'runtime' => '0777',
            'runtime' . DIRECTORY_SEPARATOR . 'logs' => '0777',
            'web' . DIRECTORY_SEPARATOR . 'assets' => '0777',
            'yii' => '0755',
            'config/config.ini.php' => '0666',
        ];
    }

    /**
     * Get list of additional fields. Use this method for add your additional fields.
     * Example method return:
     * ```
     * [
     *     [
     *         'main_module' => 'customers',
     *         'module' => 'name_of_your_add_on',
     *         'name' => 'additional_filed_name',
     *         'required' => 'false',
     *         'addon_uri' => '/url_to_process_this_afs',
     *     ]
     * ]
     * ```
     * @return array List of additional fields
     */
    public function getAdditionalFields()
    {
        return [];
    }

    /**
     * Get list of hooks. Use this method for add your hooks to relynt.
     * For example the return array:
     * ```
     * [
     *     [
     *         'title' => 'My hook',
     *         'enabled' => 'true',
     *         'type' => 'cli', // `cli` or `queue`
     *         'path' => '/var/www/test.php',
     *         'events' => [
     *             [
     *                 'model' => 'models\common\customers\Customers',
     *                 'actions' => ['edit']
     *             ],
     *             [
     *                 'model' => 'models\admin\administration\Locations',
     *                 'actions' => ['create', 'delete']
     *             ],
     *         ]
     *     ]
     * ]
     * ```
     * @return array List of hooks
     */
    public function getHooks()
    {
        return [];
    }

    /** @var string $_baseIp - Used for save getBaseIp method result */
    protected static $_baseIp;

    /**
     * Get IP of current server.
     * @return string
     */
    protected static function getBaseIp()
    {
        if (empty(static::$_baseIp)) {
            static::$_baseIp = IPHelper::getFirstIP();
        }

        return static::$_baseIp;
    }

    protected static $_domain;

    protected static function getDomain()
    {
        if (empty(static::$_domain)) {
            static::$_domain = IPHelper::getDomain();
        }

        return static::$_domain;
    }

    /**
     * Get path to param.php file.
     * @return string
     */
    protected static function getParamsFilePath()
    {
        return static::getBaseDir() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.php';
    }

    /**
     * Get path to params.example.php file.
     * @return string
     */
    protected static function getBaseParamsFilePath()
    {
        return static::getBaseDir() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.example.php';
    }

    /**
     * Create encryption points.
     * @return bool
     */
    protected function createEntryPoints()
    {
        $points = $this->getEntryPoints();
        if (empty($points)) {
            return true;
        }

        $relyntDir = static::getRelyntDir();

        foreach ($points as $point) {
            $command = "{$relyntDir}system/script/addon add-or-get-entry-point";
            $properties = ArrayHelper::merge($this->getBaseEntryPointProperties(), $point);

            foreach ($properties as $key => $value) {
                if ($key == 'url' or $key == 'code') {
                    /**@see  https://jira.relynt.com/browse/SAHELPER-25 */
                    $testEncodedValue = urlencode(urldecode($value));
                    if ($testEncodedValue !== $value) {
                        //string is NOT urlencoded and need to urlencode
                        $value = urlencode($value);
                    }
                }
                $command .= " --$key=\"$value\"";
            }

            $result = exec($command);
            if (!$result) {
                return false;
            }
        }

        return true;
    }

    /**
     * Properties used in all entry points.
     * @return array
     */
    protected function getBaseEntryPointProperties()
    {
        return [
            'module' => $this->getModuleName(),
        ];
    }

    /**
     * Create add-on module by name.
     * @return bool
     */
    protected function createModule()
    {
        $relyntDir = static::getRelyntDir();
        $moduleName = $this->getModuleName();
        $title = $this->getAddOnTitle();
        $baseDir = static::getBaseDir();

        $moduleStatus = $this->module_status ? "--status=\"{$this->module_status}\"" : '';

        $result = exec("{$relyntDir}system/script/addon add-or-get-module --module=\"$moduleName\" --title=\"$title\" --path=\"$baseDir\" {$moduleStatus}");

        if (trim($result) != $moduleName) {
            return false;
        }

        return true;
    }

    /**
     * Add current IPs to API key white list
     * @param $apiKeyId
     */
    public function actionAddIpsToWhiteList($apiKeyId)
    {
        // Add IPs to white list
        exec(static::getRelyntDir() . 'system/script/addon api-key-white-list --id=' . $apiKeyId . ' --list="' . implode(',', IPHelper::getIPs()) . '"');
    }

    /**
     * Get api key data by ID. Get false when error.
     * @param string|int $id - Api key ID.
     * @return array|bool
     */
    protected static function getApiKeyAndSecretById($id)
    {
        $result = exec(static::getRelyntDir() . 'system/script/addon get-api-key-and-secret --id=' . $id);
        if (!$result) {
            return false;
        }

        list($key, $secret) = explode(',', $result);

        return [
            'key' => $key,
            'secret' => $secret,
        ];
    }

    /**
     * Set api key permissions by id.
     * @param string|int $apiKeyId
     */
    protected function setApiPermissions($apiKeyId)
    {
        $permissions = $this->getApiPermissions();
        if (!empty($permissions)) {
            $relyntDir = static::getRelyntDir();

            foreach ($permissions as $permission) {
                $controller = $permission['controller'];
                $actions = isset($permission['actions']) ? $permission['actions'] : ['index'];
                $rule = isset($permission['rule']) ? $permission['rule'] : static::DEFAULT_PERMISSION_RULE;

                if (!is_array($actions)) {
                    $actions = [$actions];
                }

                foreach ($actions as $action) {
                    exec("{$relyntDir}system/script/addon set-api-key-permission --id=\"$apiKeyId\" --controller=\"$controller\" --action=\"$action\" --rule=\"$rule\"");
                }
            }
        }
    }

    /**
     * Create api key in Relynt for current add-on.
     * Add-on title gets from static::getAddOnTitle() method.
     * @return int - Api key ID.
     */
    protected function createApiKey()
    {
        $command = static::getRelyntDir() . 'system/script/addon add-or-get-api-key --title="' . $this->getAddOnTitle() . '"';
        return (int)exec($command);
    }

    /**
     * Get Relynt global path
     * @return string - Global path to Relynt
     */
    protected static function getRelyntDir()
    {
        return '/var/www/relynt/';
    }

    /**
     * Get add-on base dir
     * @return string
     */
    protected static function getBaseDir()
    {
        return Yii::$app->getBasePath();
    }

    /**
     * Check Relynt version
     * @return bool
     */
    protected static function checkRelyntVersion()
    {
        if (empty(static::$minimumRelyntVersion)) {
            return true;
        }

        if ($relyntVersion = ConfigHelper::getSpynxVersion()) {
            if (version_compare($relyntVersion, static::$minimumRelyntVersion) === -1) {
                return false;
            }
        } else {
            echo 'Version file does not exist! Your Relynt version must be higher than ' . static::$minimumRelyntVersion . "!\n";
        }

        return true;
    }

    /**
     * Generate random encryption key.
     * @return string
     * @throws yii\base\Exception
     */
    protected static function generateEncryptionKey()
    {
        return Yii::$app->getSecurity()->generateRandomString(rand(20, 30));
    }

    /**
     * Set permissions to files or directories.
     * @param array $paths - Key of array item is relative path to file or directory. Value of array item is permission.
     */
    protected static function chmodFiles($paths)
    {
        if (empty($paths)) {
            return;
        }

        $baseDir = static::getBaseDir();

        foreach ($paths as $path => $permission) {
            $absolutePath = $baseDir . DIRECTORY_SEPARATOR . $path;

            echo "chmod('$absolutePath', $permission)...";

            if (is_dir($absolutePath) || is_file($absolutePath)) {
                try {
                    if (chmod($absolutePath, octdec($permission))) {
                        echo "done.\n";
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            } else {
                echo "file not found.\n";
            }
        }
    }

    /**
     * Set initial params to Relynt config and create params.php file with default params.
     * By default these setting will be added to params.php file:
     * 1. add_on_encryption_key - Used for encrypted settings.
     * 2. cookieValidationKey - Need for identify our cookies.
     * 3. api_domain
     * 4. api_key
     * 5. api_secret
     * @param string $apiKeyKey
     * @param string $apiKeySecret
     * @throws yii\base\Exception
     */
    protected function prepareParamsFile($apiKeyKey, $apiKeySecret)
    {
        $relyntDir = static::getRelyntDir();
        $moduleName = $this->getModuleName();
        if (($domain = static::getDomain()) !== null) {
            $url = 'https://' . $domain . '/';
        } else {
            $url = 'http://' . static::getBaseIp() . '/';
        }

        if (file_exists(static::getParamsFilePath())) {
            $this->actionGenerateKey();

            // Move new settings from params.example.php to params.php
            $this->mergeDefaultParams();

            // Reload add-on configs
            exec($relyntDir . 'system/setup/setup reload-configs');

            // Try save params to relynt config
            // Check if API domain is already set
            if (empty(ConfigHelper::get('api_domain'))) {
                // Set API IP
                exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_domain" --value="' . $url . '"');
            }
            exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_key" --value="' . $apiKeyKey . '"');
            exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_secret" --value="' . $apiKeySecret . '"');

            echo 'Params file already exists! Terminate!' . "\n";
            return;
        }

        $defaultRequiredParams = [
            static::ENCRYPTION_KEY_FIELD => static::generateEncryptionKey(),
            static::COOKIE_VALIDATION_KEY_FIELD => Yii::$app->getSecurity()->generateRandomString(),
            static::API_DOMAIN_FIELD => $url,
            static::API_KEY_FIELD => $apiKeyKey,
            static::API_SECRET_FIELD => $apiKeySecret,
        ];

        $paramsFilePath = static::getParamsFilePath();
        // Create params.php
        copy(static::getBaseParamsFilePath(), $paramsFilePath);

        // Set default params
        foreach ($defaultRequiredParams as $key => $value) {
            $comment = 'It is required default param';
            static::insertParamToFile($paramsFilePath, $key, $value, $comment);
        }

        // Reload add-on configs
        exec($relyntDir . 'system/setup/setup reload-configs');

        // Try save params to relynt config
        exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_domain" --value="' . $url . '"');
        exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_key" --value="' . $apiKeyKey . '"');
        exec($relyntDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_secret" --value="' . $apiKeySecret . '"');
    }

    /**
     * Set param to php array file
     * @param string $paramsFilePath Path to params file
     * @param string $key
     * @param string $value
     * @param string $itemComment Comment which inserted before param string (Only for new params)
     * @return bool
     */
    protected function insertParamToFile($paramsFilePath, $key, $value, $itemComment = '')
    {
        if (!file_exists($paramsFilePath)) {
            return false;
        }

        $paramsArray = require($paramsFilePath);
        $fileContent = file_get_contents($paramsFilePath);

        $valueAsPhpCode = var_export($value, 1);

        if (isset($paramsArray[$key])) {
            $fileContent = preg_replace('/(("|\')' . $key . '("|\')\s*=>\s*)(""|\'\')/', "\\1$valueAsPhpCode", $fileContent);
        } else {
            if (!empty($itemComment)) {
                $itemComment = '// ' . $itemComment;
            }

            $insertingText = <<<TEXT
\$0
    $itemComment
    '$key' => $valueAsPhpCode,
TEXT;
            $fileContent = preg_replace('/return[\n\s\t]*(\s*\[|array\s*\()/im', $insertingText, $fileContent);
        }

        file_put_contents($paramsFilePath, $fileContent);

        return true;
    }

    /**
     * Move new params from params.example.php to params.php
     */
    protected function mergeDefaultParams()
    {
        $defaultParamsFile = static::getBaseParamsFilePath();
        $paramsFile = static::getParamsFilePath();
        $defaultParamsArray = require($defaultParamsFile);
        $paramsArray = require($paramsFile);

        foreach ($defaultParamsArray as $key => $value) {
            if (!array_key_exists($key, $paramsArray)) {
                $this->insertParamToFile($paramsFile, $key, $value);
            }
        }
    }

    /**
     * Create additional fields from getAdditionalFields method.
     * @return bool false if failure
     */
    protected function createAdditionalFields()
    {
        $fields = $this->getAdditionalFields();

        if (empty($fields)) {
            return true;
        }

        $relyntDir = static::getRelyntDir();

        foreach ($fields as $fieldData) {
            $command = "{$relyntDir}system/script/addon add-or-get-additional-field ";

            foreach ($fieldData as $key => $value) {
                $command .= " --$key=\"$value\"";
            }

            if (!exec($command)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create hooks with events from getHooks method.
     * @return bool False on failure
     */
    protected function createHooks()
    {
        $hooks = $this->getHooks();

        if (empty($hooks)) {
            return true;
        }

        $relyntDir = static::getRelyntDir();

        foreach ($hooks as $hook) {
            $command = "{$relyntDir}system/script/addon add-or-get-hook ";

            $events = isset($hook['events']) ? $hook['events'] : [];
            unset($hook['events']);

            foreach ($hook as $key => $value) {
                $command .= " --$key=\"$value\"";
            }

            $hookId = exec($command);

            if ($hookId == '0') {
                return false;
            }

            if (!$this->createHookEvents($hookId, $events)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create hook events.
     * Example $events list:
     * [
     *      [
     *          'model' => 'models\common\customers\Customers',
     *          'actions' => ['create', 'edit']
     *      ]
     * ]
     * @param int $hookId Hook id
     * @param array $events Events list
     * @return bool False on failure
     */
    protected function createHookEvents($hookId, $events)
    {
        if (empty($hookId) or empty($events)) {
            return true;
        }

        $relyntDir = static::getRelyntDir();

        $createEventBaseCommand = "{$relyntDir}system/script/addon add-or-get-hook-event --id=\"$hookId\"";

        foreach ($events as $event) {
            if (!isset($event['model']) or !isset($event['actions'])) {
                return false;
            }

            foreach ($event['actions'] as $action) {
                $command = "$createEventBaseCommand --model=\"{$event['model']}\" --action=\"$action\"";

                if (exec($command) == '0') {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // Check Relynt version
        if (!static::checkRelyntVersion()) {
            exit("Error: Your Relynt version is very old!\nMinimum required Relynt version: " . static::$minimumRelyntVersion . "!\n");
        }

        $process = exec(static::getRelyntDir() . 'system/script/addon check-module --name="' . $this->getModuleName() . '"');
        $this->process = $process == '1' ? static::UPDATE_PROCESS : static::INSTALL_PROCESS;

        return parent::beforeAction($action);
    }

    protected static function checkIfModuleInstalled($module)
    {
        return (bool)exec(static::getAddOnScriptPath(' check-module --name="' . $module . '"'));
    }

    protected function checkIfThisModuleInstalled()
    {
        return static::checkIfModuleInstalled($this->getModuleName());
    }

    /**
     * Check or it is install process
     * @return bool
     */
    public function isInstallProcess()
    {
        return $this->process == static::INSTALL_PROCESS;
    }

    /**
     * Check or it is update process
     * @return bool
     */
    public function isUpdateProcess()
    {
        return $this->process == static::UPDATE_PROCESS;
    }

    /**
     * Get path to add-on script
     * @param string $param String to add
     * @return string
     */
    private static function getAddOnScriptPath($param = '')
    {
        return static::getRelyntDir() . 'system/script/addon' . $param;
    }

    protected $_created_payment_accounts = [];

    /**
     * Create payment accounts
     * @return bool
     */
    public function createPaymentAccounts()
    {
        $accounts = $this->getPaymentAccounts();

        if (empty($accounts)) {
            return true;
        }

        foreach ($accounts as $account) {
            $command = static::getAddOnScriptPath(' add-or-get-payment-account ');

            foreach ($account as $key => $value) {
                $command .= ' --' . $key . '="' . $value . '"';
            }

            $result = exec($command);
            if (!$result) {
                return false;
            }

            $this->_created_payment_accounts[] = (int)$result;
        }

        return true;
    }

    /**
     * Config data for payment accounts.
     * Example:
     *  return [
     *      [
     *          'title' => 'SEPA',
     *          'field_1' => 'IBAN',
     *      ]
     *  ];
     * @return array
     */
    public function getPaymentAccounts()
    {
        return [];
    }

    protected function configSet($key, $value)
    {
        exec(static::getAddOnScriptPath(' config-set --addon="' . $this->getModuleName() . '" --key="' . $key . '" --value="' . $value . '"'));
    }

    /**
     * Array with financial handlers to remove
     *
     * Than this method must return array like this:
     *      return [
     *          'export' => ['export-handler'],
     *          'charge' => ['charge-handler'],
     *      ];
     *
     * @return array
     */
    public function getFinancialHandlersForRemove()
    {
        return [
            self::FINANCE_CHARGE_HANDLERS => [],
            self::FINANCE_EXPORT_HANDLERS => [],
            self::FINANCE_BANK_STATEMENS_HANDLERS => [],
        ];
    }

    /**
     * Remove financial handlers
     * @return bool
     * @throws yii\base\ErrorException
     */
    private function removeFinancialHandlers()
    {
        $handlersAll = $this->getFinancialHandlersForRemove();

        if (!is_array($handlersAll)) {
            return true;
        }

        foreach ($handlersAll as $type => $handlers) {
            if (($type != self::FINANCE_CHARGE_HANDLERS && $type != self::FINANCE_EXPORT_HANDLERS && $type != self::FINANCE_BANK_STATEMENS_HANDLERS) || empty($handlers)) {
                continue;
            }

            foreach ($handlers as $handler) {
                FileHelper::removeDirectory(static::getRelyntDir() . 'system/external_handlers/finance/' . $type . '/' . $handler);
            }
        }

        return true;
    }

    /**
     * Array with financial handlers to copy
     *
     * In add-on you must have directory `handler` with directory (or a few) that contains handlers files.
     * Fox example two handlers: `relynt-addon/handler/export-handler` and `relynt-addon/handler/charge-handler`.
     * Each contains all files that it needs.
     *
     * Than this method must return array like this:
     *      return [
     *          'export' => ['export-handler'],
     *          'charge' => ['charge-handler'],
     *      ];
     *
     * @return array
     */
    public function getFinancialHandlers()
    {
        return [
            self::FINANCE_CHARGE_HANDLERS => [],
            self::FINANCE_EXPORT_HANDLERS => [],
            self::FINANCE_BANK_STATEMENS_HANDLERS => [],
        ];
    }

    /**
     * Copy financial handlers
     * @return bool
     */
    private function copyFinancialHandlers()
    {
        $handlersAll = $this->getFinancialHandlers();

        if (!is_array($handlersAll)) {
            return true;
        }

        foreach ($handlersAll as $type => $handlers) {
            if ($type != self::FINANCE_CHARGE_HANDLERS and $type != self::FINANCE_EXPORT_HANDLERS and $type != self::FINANCE_BANK_STATEMENS_HANDLERS) {
                continue;
            }

            if (empty($handlers)) {
                continue;
            }

            foreach ($handlers as $handler) {
                $handlerSourceDir = static::getBaseDir() . '/handler/' . $handler;
                $handlerDestinationDir = static::getRelyntDir() . 'system/external_handlers/finance/' . $type;
                echo "\nInstalling handler `$handler`...\n";
                if (!is_writable($handlerDestinationDir)) {
                    print "Warning! Can't copy files!\n";
                    print 'You must manually copy content of dir `' . $handlerSourceDir . '` to `' . $handlerDestinationDir . '`' . "\n";
                } else {
                    $handlerDestinationDir .= '/' . $handler;
                    print "Coping handler's files...\n";
                    FileHelper::copyDirectory($handlerSourceDir, $handlerDestinationDir);
                    print "Handler installing done!\n";
                }
            }
        }

        return true;
    }

    /**
     * Get list of simple|extended|add-on modules. Use this method for adding needed for add-on modules
     * For example return array:
     * ```
     * [
     *      [
     *          'module' => 'test_simple_module',
     *          'type' => ConsoleModuleConfig::TYPE_SIMPLE,
     *          'root' => 'controllers\admin\config\NetworkingController',
     *          'icon' => 'fa-cog',
     *          'title' => 'Test simple module',
     *      ]
     * ]
     * ```
     * @return array
     */
    public function getModulesList()
    {
        return [];
    }

    /**
     * Create simple|extended|add-on modules
     * @return bool
     */
    public function createModules()
    {
        $modules = $this->getModulesList();

        if (empty($modules)) {
            return true;
        }

        foreach ($modules as $item) {
            // check if exist
            $module = (new ConsoleModuleConfig())->findOne([
                'module' => $item['module'],
                'type' => $item['type'],
            ]);

            if (!empty($module)) {
                continue;
            }

            $module = new ConsoleModuleConfig($item);
            if (!$module->save()) {
                return false;
            }
        }

        return true;
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        // Clear Redis cache
        exec(static::getRelyntDir() . 'system/script/tools reset-redis-cache > /dev/null 2>&1 3>/dev/null');

        return $result;
    }
}
