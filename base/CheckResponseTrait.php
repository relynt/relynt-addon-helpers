<?php

namespace relynt\base;

use Yii;

/**
 * Trait CheckResponseTrait
 * @package relynt\base
 */
trait CheckResponseTrait
{
    /**
     * @param string|array $response
     * @throws ApiResponseException
     */
    public function checkResponseForError($response)
    {
        if (empty($response)) {
            return;
        }

        if (isset($response['error'])) {
            throw new ApiResponseException(
                Yii::t('app', 'System return response with exeption. Message: "{message}". Internal code: {internal_code}', [
                    'message' => $response['error']['message'],
                    'internal_code' => $response['error']['internal_code'],
                ]),
                $response['error']['code']
            );
        }
    }
}
