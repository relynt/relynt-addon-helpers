<?php

namespace relynt\base;

use yii\base\Exception;

/**
 * Class ApiResponseException
 * @package relynt\v2\base
 */
class ApiResponseException extends Exception
{
}
