<?php

namespace relynt\base;

use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Class BaseConsoleModel
 *
 * @property bool $isNewRecord Whether the record is new and should be inserted when calling [[save()]].
 * @package relynt\base
 */
class BaseConsoleModel extends Model
{
    /**
     * Minimum Relynt version needs for this add-on. If empty then version will not check.
     * @var string
     */
    public static $minimumRelyntVersion = '3.1';

    /**
     * Controller name for working with model
     * @var string
     */
    public static $controllerName;

    /**
     * Old attribute values indexed by attribute names.
     * @var array|null
     * This is `null` if the record [[isNewRecord|is new]].
     */
    protected $_oldAttributes;

    /**
     * This is property for saving some error message from  relynt
     * @var string
     */
    private $_errorResponseMessage;

    // default actions
    const ACTION_INDEX = 'index';
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_VIEW = 'view';
    const ACTION_DELETE = 'delete';

    /**
     * Set attributes from  array to model properties
     *
     * @param static $model
     * @param array $data
     */
    public static function populate($model, $data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($model, $name)) {
                $model->$name = $value;
            }
        }
        $model->_oldAttributes = $model->attributes;
    }

    /**
     * Get relynt global path
     *
     * @return string
     */
    protected static function getRelyntDir()
    {
        return '/var/www/relynt/';
    }

    /**
     * Create url, set controller action  and encode params
     *
     * @param string $action
     * @param array $params
     * @return string
     */
    protected function createUrl($action = null, array $params = [])
    {
        return $this->getBaseConsolePath() . ' ' . static::$controllerName . (!empty($action) ? '--' . $action : '') . ' --params=\'' . json_encode($params) . '\'';
    }

    /**
     * Get relynt console api path
     *
     * @return string
     */
    protected function getBaseConsolePath()
    {
        return static::getRelyntDir() . 'system/script/console_api';
    }

    protected static $primaryKeys = ['id'];

    /**
     * Get model`s primary key
     *
     * @return array
     */
    public function getPrimaryKey()
    {
        return static::$primaryKeys;
    }

    /**
     * Returns a value indicating whether the current record is new.
     *
     * @return bool whether the record is new and should be inserted when calling [[save()]].
     */
    public function getIsNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * Check result, if result true we set old attributes, else we check if is errors or response message
     *
     * @param array $result
     */
    protected function checkResult($result)
    {
        if ($result['result'] == true) {
            $this->_oldAttributes = $this->attributes;
        } else {
            if (is_array($result['response'])) {
                foreach ($result['response'] as $item) {
                    $this->addError($item['field'], $item['message']);
                }
            } else {
                $this->_errorResponseMessage = $result['response'];
            }
        }
    }

    /**
     * Get primary keys values
     *
     * @return null|mixed
     */
    private function getPrimaryKeysValues()
    {
        $attributes = $this->getAttributes();
        $primaryKeys = $this->getPrimaryKey();
        $primaryKeysValues = [];

        foreach ($primaryKeys as $key) {
            if (isset($attributes[$key]) and $attributes[$key] !== null) {
                $primaryKeysValues[$key] = $attributes[$key];
            }
        }

        return $primaryKeysValues;
    }

    /**
     * Method is called before saving model
     */
    public function beforeSave()
    {
    }

    /**
     * Save model
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        $this->beforeSave();
        return $this->isNewRecord ? $this->create($runValidation) : $this->update($runValidation);
    }

    /**
     * Create record
     *
     * @param bool $runValidation
     * @return bool
     */
    public function create($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $command = $this->createUrl(static::ACTION_CREATE, $this->getAttributesForSend());
        $result = $this->exec($command);

        // Set primary key value after create record
        if ($result['result'] == true and !empty($result['response'])) {
            foreach ($this->getPrimaryKey() as $key) {
                $this->$key = $result['response'][$key];
            }
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Update record
     *
     * @param bool $runValidation
     * @return bool
     */
    public function update($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $params = $this->getAttributesForSend();
        // add primary keys values to attributes if it is not set
        foreach ($this->getPrimaryKeysValues() as $key => $value) {
            if (!isset($params[$key])) {
                $params[$key] = $value;
            }
        }

        $command = $this->createUrl(static::ACTION_UPDATE, $params);
        $result = $this->exec($command);
        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Get attributes for send.
     * If it is new record we return all attributes else return only changed attributes
     *
     * @return array
     */
    public function getAttributesForSend()
    {
        $attributes = $this->getAttributes($this->attributes());

        $additional_attributes = null;
        if (array_key_exists('additional_attributes', $attributes)) {
            $additional_attributes = $attributes['additional_attributes'];
            unset($attributes['additional_attributes']);
        }

        $attributesForSend = [];
        if ($this->isNewRecord) {
            //Add not empty attributes for send
            foreach ($attributes as $name => $value) {
                if ($value !== null) {
                    $attributesForSend[$name] = $value;
                }
            }
        } else {
            //Add changed attributes for send
            foreach ($attributes as $name => $value) {
                if ((array_key_exists($name, $this->_oldAttributes) && $value !== $this->_oldAttributes[$name])) {
                    $attributesForSend[$name] = $value;
                }
            }
        }

        if ($additional_attributes !== null) {
            $attributesForSend['additional_attributes'] = $additional_attributes;
        }

        return $attributesForSend;
    }

    /**
     * Delete record
     *
     * @return bool
     */
    public function delete()
    {
        $primaryKeysValues = $this->getPrimaryKeysValues();
        if (empty($primaryKeysValues)) {
            return false;
        }

        $command = $this->createUrl(static::ACTION_DELETE, $primaryKeysValues);

        $result = $this->exec($command);

        if (empty($result)) {
            return false;
        }

        if (isset($result['result']) and $result['result'] === true) {
            $this->_oldAttributes = null;
        } else {
            $this->checkResult($result);
        }

        return $result['result'];
    }

    /**
     * @param string $action
     * @param array $params
     * @return mixed
     */
    public function customAction($action, $params = [])
    {
        $command = $this->createUrl($action, $params);
        $result = $this->exec($command);
        $this->checkResult($result);

        return $result['response'];
    }

    /**
     * Run command and decode result
     *
     * @param string $command
     * @return array|string|null
     */
    protected function exec($command)
    {
        // Check if console api file is exist
        $this->checkConsoleApi();

        $result = shell_exec($command . " 2>&1");
        $resultArray = json_decode($result, 1);

        if ($resultArray == null) {
            return [
                'result' => false,
                'response' => $result
            ];
        }

        return $resultArray;
    }

    /**
     * Check if console api file is exist
     */
    protected function checkConsoleApi()
    {
        if (!file_exists($this->getBaseConsolePath())) {
            exit("Error: Your Relynt version is old for this add-on!\nMinimum required Relynt version: " . static::$minimumRelyntVersion . "!\nPlease upgrade your system\n");
        }
    }

    /**
     * Find record by conditions
     *
     * @param array $conditions
     * @param bool $one
     * @return array|mixed|null
     */
    private function find(array $conditions, $one)
    {
        $command = $this->createUrl(static::ACTION_INDEX, $conditions);
        $result = $this->exec($command);
        $this->checkResult($result);

        if ($result['result'] == false or empty($result['response'])) {
            return $one ? null : [];
        }

        $models = [];
        foreach ($result['response'] as $item) {
            $model = new static();
            static::populate($model, $item);
            $models[] = $model;
        }

        return $one ? reset($models) : $models;
    }

    /**
     * Find model by primary key, if model has several primary keys need set array with pk values
     * Example:
     *          HookEvents has 3 pk (id, model, action) then function call must be:
     *          $model->findByPk([
     *              'id' => 5,
     *              'model' => 'models\common\finance\Transactions',
     *              'action' => 'edit'
     *          ]);
     * @param array $pkValues
     * @return null|static
     */
    public function findByPk($pkValues = [])
    {
        $command = $this->createUrl(static::ACTION_VIEW, $pkValues);

        $result = $this->exec($command);
        if ($result['result'] == false or empty($result['response'])) {
            $this->checkResult($result);
            return null;
        }

        $model = new static();
        static::populate($model, $result['response']);

        return $model;
    }

    /**
     * Combine conditions for search
     *
     * @param array $mainAttributes
     * @param array $additionalAttributes
     * @param array $order
     * @param integer|null $limit
     * @return array
     */
    private function combineSearchConditions(array $mainAttributes = [], array $additionalAttributes = [], array $order = [], $limit = null)
    {
        $result = [];
        if (!empty($mainAttributes)) {
            $result['main_attributes'] = $mainAttributes;
        }
        if (!empty($additionalAttributes)) {
            $result['additional_attributes'] = $additionalAttributes;
        }
        if (!empty($order)) {
            $result['search_conditions']['order'] = $order;
        }
        if ($limit !== null) {
            if (!is_int($limit)) {
                throw new InvalidParamException("limit must be integer");
            }
            $result['search_conditions']['limit'] = $limit;
        }

        return $result;
    }

    /**
     * Find one record by conditions
     *
     * @param array $mainAttributes
     * @param array $additionalAttributes
     * @param array $order
     * @return $this
     */
    public function findOne(array $mainAttributes, array $additionalAttributes = [], $order = [])
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, 1), true);
    }

    /**
     * Find all record by conditions
     *
     * @param array $mainAttributes
     * @param array $additionalAttributes
     * @param array $order
     * @param int $limit
     * @return $this[]
     */
    public function findAll(array $mainAttributes, array $additionalAttributes = [], array $order = [], $limit = null)
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, $limit), false);
    }

    /**
     * Get response errors
     *
     * @return string
     */
    public function getResponseErrorMesage()
    {
        return $this->_errorResponseMessage;
    }
}
