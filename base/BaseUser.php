<?php

namespace relynt\base;

class BaseUser extends \yii\web\User
{
    public function loginRequired($checkAjax = true, $checkAcceptHeader = true)
    {
        parent::loginRequired($checkAjax, $checkAcceptHeader);

        return \Yii::$app->getResponse()->redirect('/portal/login/?return=' . \Yii::$app->getRequest()->getUrl());
    }
}
