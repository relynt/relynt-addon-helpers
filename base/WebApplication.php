<?php

namespace relynt\base;

use relynt\helpers\ConfigHelper;
use relynt\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\web\Application;

/**
 * Class WebApplication is the base web application for Relynt Add-Ons
 * @package relynt\base
 */
class WebApplication extends Application
{
    public $api;

    /**
     * WebApplication constructor.
     * @param string $baseDir Path to add-on directory
     * @param string $configPath Path to config file
     * @throws InvalidConfigException
     */
    public function __construct($baseDir, $configPath)
    {
        $config = ConfigHelper::getWebConfig($baseDir, $configPath);
        register_shutdown_function([ApiHelper::class, 'logoutBeforeDie']);
        parent::__construct($config);
    }
}
