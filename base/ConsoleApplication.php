<?php

namespace relynt\base;

use relynt\helpers\ConfigHelper;
use relynt\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\console\Application;

/**
 * Class ConsoleApplication is the base console application for Relynt Add-Ons
 * @package relynt\base
 */
class ConsoleApplication extends Application
{
    public $api;

    /**
     * ConsoleApplication constructor.
     * @param string $baseDir Path to add-on directory
     * @param string $configPath Path to config file
     * @throws InvalidConfigException
     */
    public function __construct($baseDir, $configPath)
    {
        $config = ConfigHelper::getConsoleConfig($baseDir, $configPath);
        register_shutdown_function([ApiHelper::class, 'logoutBeforeDie']);
        parent::__construct($config);
    }
}
