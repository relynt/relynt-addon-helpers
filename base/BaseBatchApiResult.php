<?php

namespace relynt\base;

use Iterator;
use yii\base\BaseObject;

/**
 * Class BaseBatchApiResult
 * @package relynt\base
 */
class BaseBatchApiResult extends BaseObject implements Iterator
{
    public $each = false;

    /** @var BaseActiveApi $model */
    public $model;
    public $mainAttributesCondition;
    public $additionalAttributesCondition;
    public $order;

    public $batchSize = 100;

    private $_eachPosition = 0;
    private $_position = 0;
    private $_offset = 0;
    private $_data;
    private $_value;

    /**
     * Need for implement Iterator interface
     */
    public function rewind()
    {
        $this->_eachPosition = 0;
        $this->_position = 0;
        $this->_offset = 0;

        $this->_value = null;
        $this->next();
    }

    /**
     * Need for implement Iterator interface
     * @return BaseActiveApi|null
     */
    public function current()
    {
        return $this->_value;
    }

    /**
     * Need for implement Iterator interface
     * @return int
     */
    public function key()
    {
        if ($this->each) {
            return $this->_eachPosition;
        }

        return $this->_position;
    }

    /**
     * Need for implement Iterator interface
     * @return int|null
     */
    public function next()
    {
        if (!$this->each) {
            $this->_value = $this->getBatch();
            return ++$this->_position;
        }

        if (empty($this->_data) or next($this->_data) === false) {
            $this->_data = $this->getBatch();
            ++$this->_position;
        }

        $this->_value = current($this->_data);
        if (key($this->_data) !== null) {
            $this->_eachPosition++;
        } else {
            $this->_eachPosition = null;
        }

        return $this->_eachPosition;
    }

    /**
     * Need for implement Iterator interface
     * @return bool
     */
    public function valid()
    {
        return !empty($this->_value);
    }

    /**
     * Get items by conditions and with limit and offset
     * @return BaseActiveApi[]
     */
    private function getBatch()
    {
        return $this->model->findAll(
            $this->mainAttributesCondition,
            $this->additionalAttributesCondition,
            $this->order,
            $this->batchSize,
            $this->_position * $this->batchSize
        );
    }
}
