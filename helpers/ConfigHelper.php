<?php

namespace relynt\helpers;

use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\redis\Connection;

class ConfigHelper extends BaseObject
{
    const TYPE_TEXT = 'text';
    const TYPE_SELECT = 'select';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_MULTIPLE_SELECT = 'multipleSelect';
    const TYPE_ENCRYPTED = 'encrypted';
    const ADD_ON_ENCRYPTION_KEY_FIELD = 'add_on_encryption_key';
    const PARTNERS_SETTINGS_FIELD = 'partners_settings';
    const DEFAULT_PARTNERS_SETTINGS_FIELD = 'default_partners_settings';
    /** Config file name */
    const CONFIG_FILE = 'config.ini.php';
    public static $addOnPath;
    private static $_encryptionKey;
    private static $_relyntConfig;

    protected static function getParamsFilePath()
    {
        return static::$addOnPath . DIRECTORY_SEPARATOR . 'params.php';
    }

    /**
     * Load params.php file if exist
     * @return bool|array
     */
    protected static function getParamsFile()
    {
        $paramsFilePath = static::getParamsFilePath();
        if (file_exists($paramsFilePath)) {
            return require($paramsFilePath);
        }

        return false;
    }

    /**
     * Full path to Relynt version file
     * @return string
     */
    public static function getRelyntVersionPath()
    {
        return '/var/www/relynt/version';
    }

    private static $_schemes = [];

    /**
     * Load config scheme and convert to array
     * @return bool|array
     */
    public static function getConfigScheme()
    {
        if (isset(static::$_schemes[static::$addOnPath])) {
            return static::$_schemes[static::$addOnPath];
        }

        $schemaFile = static::$addOnPath . DIRECTORY_SEPARATOR . 'config.json';
        if (!file_exists($schemaFile)) {
            return false;
        }

        static::$_schemes[static::$addOnPath] = json_decode(file_get_contents($schemaFile), true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \InvalidArgumentException('json_decode error: ' . json_last_error_msg());
        }

        return static::$_schemes[static::$addOnPath];
    }

    /**
     * Get field scheme from config.json
     * @param $keyName
     * @return null|array
     */
    public static function getKeyScheme($keyName)
    {
        $scheme = static::getConfigScheme();

        foreach ($scheme['blocks'] as $block) {
            if (isset($block['items'][$keyName])) {
                return $block['items'][$keyName];
            }
        }

        return null;
    }

    /**
     * Load and prepare config file if exist
     * @return bool|array
     */
    protected static function getConfigIniFile()
    {
        $file = static::$addOnPath . DIRECTORY_SEPARATOR . self::CONFIG_FILE;
        if (!file_exists($file)) {
            return false;
        }

        $rawParams = parse_ini_file($file, true);
        if (empty($rawParams)) {
            return false;
        }

        return static::prepareRawConfig($rawParams);
    }

    /**
     * Prepare boolean value and convert settings with `key=value,` structure to array
     * @param array $config
     * @return array
     */
    protected static function prepareRawConfig($config)
    {
        $result = [];
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $result[$key] = static::prepareRawConfig($value);
                continue;
            }

            $scheme = static::getKeyScheme($key);
            if ($scheme == null or !isset($scheme['type'])) {
                $type = self::TYPE_TEXT;
            } else {
                $type = $scheme['type'];
            }

            switch ($type) {
                case self::TYPE_BOOLEAN:
                    $value = ($value == 'yes' or $value == '1') ? true : false;
                    break;
                case self::TYPE_MULTIPLE_SELECT:
                    $value = trim($value);
                    $value = empty($value) ? [] : explode(',', $value);
                    break;
            }

            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * Decrypt encrypted values
     * @param $params
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private static function prepareEncryptedFields($params)
    {
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $params[$key] = static::prepareEncryptedFields($value);
                continue;
            }
            $scheme = static::getKeyScheme($key);

            if (isset($scheme['type']) and $scheme['type'] == self::TYPE_ENCRYPTED) {
                $params[$key] = SecurityHelper::decrypt($value, static::$_encryptionKey);
            }
        }

        return $params;
    }

    /**
     * Get params with check partner ID and other expressions
     * @param string|null $addOnDir Path to add-on directory. If not given - will be used from static property
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    public static function getParams($addOnDir = null)
    {
        if ($addOnDir !== null) {
            static::$addOnPath = $addOnDir;
        }

        $oldParams = static::getParamsFile();
        $iniParams = static::getConfigIniFile();

        if ($iniParams == false) {
            return $oldParams == false ? null : $oldParams;
        }

        if ($oldParams == false) {
            $oldParams = [];
        }

        $resultParams = $iniParams['common'];
        $resultParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD] = $iniParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD];

        foreach ($iniParams as $section => $settings) {
            if ($section == 'common' or $section == self::DEFAULT_PARTNERS_SETTINGS_FIELD) {
                continue;
            }

            $resultParams[self::PARTNERS_SETTINGS_FIELD][$section] = $settings;
        }

        // Remove per partner params from old params
        $oldParams = array_diff_key($oldParams, $resultParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD]);
        // Join settings from params.php
        $resultParams = ArrayHelper::merge($oldParams, $resultParams);

        static::$_encryptionKey = isset($resultParams[self::ADD_ON_ENCRYPTION_KEY_FIELD]) ? $resultParams[self::ADD_ON_ENCRYPTION_KEY_FIELD] : '';
        return static::prepareEncryptedFields($resultParams);
    }

    public static function get($key, $partner = null)
    {
        if (isset(Yii::$app->params[self::PARTNERS_SETTINGS_FIELD][$partner][$key])) {
            return Yii::$app->params[self::PARTNERS_SETTINGS_FIELD][$partner][$key];
        } elseif (isset(Yii::$app->params[$key])) {
            return Yii::$app->params[$key];
        } elseif (isset(Yii::$app->params[self::DEFAULT_PARTNERS_SETTINGS_FIELD][$key])) {
            return Yii::$app->params[self::DEFAULT_PARTNERS_SETTINGS_FIELD][$key];
        }

        return null;
    }

    /**
     * Use this method to reload add-on config
     * @throws \yii\base\InvalidConfigException
     */
    public static function reloadConfig()
    {
        Yii::$app->params = static::getParams();
    }

    /**
     * Get add-on url rules
     * @param string $baseDir Path to add-on directory
     * @return array
     */
    public static function getUrlRules($baseDir)
    {
        $pathToUrlRules = $baseDir . '/config/url_rules.php';
        if (file_exists($pathToUrlRules)) {
            return require($pathToUrlRules);
        } else {
            return [];
        }
    }

    /**
     * Get path to Relynt Redis configuration file
     * @return string
     */
    public static function getPathToRedisConfig()
    {
        return FileHelper::normalizePath('/var/www/relynt/config/redis.php');
    }

    /**
     * Get raw Relynt Redis configuration
     * @return array|null
     */
    public static function getRedisConfig()
    {
        $pathToRedisConfig = static::getPathToRedisConfig();
        if (!file_exists($pathToRedisConfig)) {
            return null;
        }
        $config = parse_ini_file($pathToRedisConfig, true);

        return $config;
    }

    /**
     * Get redis configuration for add-ons redis component
     * @return array
     */
    public static function getRedisConfigForAddOns()
    {
        $defaultRedisConfig = [
            'hostname' => '127.0.0.1',
            'port' => 6379,
        ];

        $config = static::getRedisConfig();
        if (empty($config['server'])) {
            return $defaultRedisConfig;
        }

        $config = $config['server'];

        if (isset($config['host'])) {
            $hostName = $config['host'];
        } elseif (isset($config['hostname'])) {
            $hostName = $config['hostname'];
        } else {
            $hostName = $defaultRedisConfig['hostname'];
        }

        $resultConfig = [
            'hostname' => $hostName,
            'port' => $config['port'],
        ];
        if (!empty($config['password'])) {
            $resultConfig['password'] = $config['password'];
        }

        return $resultConfig;
    }

    public static function getRedisConfigComponent()
    {
        return array_merge([
            'class' => Connection::class
        ], ConfigHelper::getRedisConfigForAddOns());
    }

    /**
     * Get path to base web config
     * @return string
     */
    public static function getPathToBaseWebConfig()
    {
        return dirname(__DIR__) . '/config/web.php';
    }

    /**
     * Get path to base console config
     * @return string
     */
    public static function getPathToBaseConsoleConfig()
    {
        return dirname(__DIR__) . '/config/console.php';
    }

    /**
     * Get add-on name by path
     * @param string $baseDir
     * @return string
     */
    public static function getAddOnNameFromBaseDir($baseDir)
    {
        $parts = explode('/', rtrim($baseDir, '/'));
        return array_pop($parts);
    }

    /**
     * Get session settings.
     * If relynt use redis session then return settings for redis session
     * Otherwise return default Yii session settings
     * @return array
     */
    public static function getSessionConfig()
    {
        $pathToRelyntRedisConfig = static::getPathToRedisConfig();
        if (file_exists($pathToRelyntRedisConfig) and extension_loaded('redis')) {
            return [
                'class' => 'relynt\base\RedisSession',
                'redis' => ConfigHelper::getRedisConfigForAddOns(),
            ];
        } else {
            return [
                'class' => 'yii\web\Session',
            ];
        }
    }

    /**
     * Get application config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to app config file
     * @param string $baseConfigPath Path to base app config file
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private static function loadAppConfig($baseDir, $configPath, $baseConfigPath)
    {
        $params = static::getParams($baseDir . '/config');

        // Set dynamic settings to base config
        $baseConfigFunc = require($baseConfigPath);
        $baseConfig = $baseConfigFunc($params, $baseDir);

        $configFunc = require($configPath);
        $config = $configFunc($params, $baseDir);

        return ArrayHelper::mergeUniqueIntegerKeyed($baseConfig, $config);
    }

    /**
     * Get web config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to web config file
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getWebConfig($baseDir, $configPath)
    {
        return static::loadAppConfig($baseDir, $configPath, static::getPathToBaseWebConfig());
    }

    /**
     * Get console config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to console config file
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getConsoleConfig($baseDir, $configPath)
    {
        return static::loadAppConfig($baseDir, $configPath, static::getPathToBaseConsoleConfig());
    }

    /**
     * Get path for Relynt config directory
     * @return string
     */
    public static function getRelyntConfigPath()
    {
        return '/var/www/relynt/config';
    }

    /**
     * Get Relynt configs by Relynt config file name
     * @var string $fileName Relynt config file name
     * @return array
     * @throws Exception
     */
    public static function getRelyntConfig($fileName)
    {
        if (isset(static::$_relyntConfig[$fileName])) {
            return static::$_relyntConfig[$fileName];
        }

        $filePath = static::getRelyntConfigPath() . DIRECTORY_SEPARATOR . $fileName . '.php';
        if (!file_exists($filePath)) {
            throw new Exception($filePath . ' file not found.');
        }

        return static::$_relyntConfig[$fileName] = parse_ini_file($filePath, true);
    }

    /**
     * Get Relynt timeZone
     * @return string Relynt timeZone
     * @throws Exception
     */
    public static function getRelyntTimeZone()
    {
        $relyntConfig = static::getRelyntConfig('config');

        if (isset($relyntConfig['relynt']['timezone'])) {
            return $relyntConfig['relynt']['timezone'];
        } elseif (ini_get('date.timezone')) {
            return ini_get('date.timezone');
        }

        return 'UTC';
    }

    /**
     * @return bool|string
     */
    public static function getSpynxVersion()
    {
        if (file_exists(static::getRelyntVersionPath())) {
            return file_get_contents(static::getRelyntVersionPath());
        }
        return false;
    }
}
