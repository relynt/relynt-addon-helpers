<?php

namespace relynt\helpers;

use DateTime;
use relynt\models\console_api\config\ConsoleRelyntConfig;

class DateHelper
{
    protected static $_dateFormats = [
        'php' => null,
        'js' => null,
    ];

    protected static $_firstDateOfTheWeek;

    public static function isNull($date)
    {
        if ($date == '0000-00-00' or $date == '0000-00-00 00:00:00') {
            return true;
        } else {
            return false;
        }
    }

    public static function validate($date, $format = 'Y-m-d')
    {
        // Check empty date
        if (static::isNull($date)) {
            return true;
        }
        $d = \DateTime::createFromFormat($format, $date);
        $result = $d && $d->format($format) == $date;
        return $result;
    }

    public static function validatePeriod($date1, $date2, $allowSameDay = false)
    {
        if (static::validate($date1) == false or static::validate($date2) == false) {
            return false;
        }

        // Check empty date
        if (static::isNull($date2)) {
            return true;
        }

        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);

        if ($allowSameDay == true) {
            if ($datetime1 <= $datetime2) {
                return true;
            }
        } else {
            if ($datetime1 < $datetime2) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public static function getJsRelyntDateFormat()
    {
        if (static::$_dateFormats['js'] === null) {
            static::$_dateFormats['js'] = static::getRelyntDateFormat();
            static::$_dateFormats['js'] = static::convertDateTimeFormatToJsStyle(static::$_dateFormats['js']);
        }
        return static::$_dateFormats['js'];
    }

    /**
     * This method converts php dateTimeFormat to js datetimeFormat.
     * @param string $format
     * @return string
     */
    protected static function convertDateTimeFormatToJsStyle($format)
    {
        return str_replace(['H', 'h', 'i', 's', 'd', 'm', 'Y', 'y'], ['HH', 'hh', 'mm', 'ss', 'DD', 'MM', 'YYYY', 'YY'], $format);
    }

    /**
     *  Method load date format from Relynt config.
     * @return string
     */
    public static function getRelyntDateFormat()
    {
        if (static::$_dateFormats['php'] === null) {
            $config = (new ConsoleRelyntConfig())->findOne(['path' => 'system', 'module' => 'main', 'key' => 'date_format']);
            static::$_dateFormats['php'] = !empty($config) ? $config->value : 'Y-m-d';
        }

        return static::$_dateFormats['php'];
    }

    /**
     * Method load first day of the week from Relynt config.
     * @return string
     */
    public static function getRelyntFirstDayOfTheWeek()
    {
        if (static::$_firstDateOfTheWeek === null) {
            $config = (new ConsoleRelyntConfig())->findOne(['path' => 'system', 'module' => 'main', 'key' => 'first_day']);
            static::$_firstDateOfTheWeek = !empty($config) ? (int)$config->value : 1;
        }

        return static::$_firstDateOfTheWeek;
    }
}
