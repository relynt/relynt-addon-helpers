<?php

namespace relynt\helpers;

use yii\base\BaseObject;

/**
 * Class BaseChargeHelper
 * @package relynt\helpers
 */
class BaseChargeHelper extends BaseObject
{
    public $id;
    public $type;

    const TYPE_INVOICE = 'invoice';

    const ACTION_UPDATE_CHARGE_PROGRESS = 'update-charge-progress';
    const ACTION_CHANGE_CHARGE_FILE_EXTENSION = 'change-charge-file-extension';

    protected static $baseCommand = '$(which php) /var/www/relynt/system/script/tools ';

    public function __construct($type, $id, array $config = [])
    {
        $this->type = $type;
        $this->id = $id;

        parent::__construct($config);
    }

    /**
     * Exec command
     * @param $command
     */
    protected function exec($command)
    {
        exec($command);
    }

    /**
     * Create command
     * @param string $action
     * @param array $params
     * @return string
     */
    protected function createCommand($action, $params = [])
    {
        $cmd = $action . ' --type="' . $this->type . '" --id="' . $this->id . '" ';

        foreach ($params as $key => $param) {
            $cmd .= '--' . $key . '="' . $param . '"';
        }

        return static::$baseCommand . $cmd;
    }

    /**
     * Get command for update charge progress
     * @param $amount
     * @return string
     */
    protected function getUpdateProgressCommand($amount)
    {
        return $this->createCommand(self::ACTION_UPDATE_CHARGE_PROGRESS, [
            'amount_ready' => $amount,
        ]);
    }

    /**
     * Increase amount charged invoices
     * @param int $amount
     */
    public function updateReady($amount)
    {
        $cmd = $this->getUpdateProgressCommand($amount);

        static::exec($cmd);
    }

    /**
     * Get command for change extension
     * @param string $extension
     * @return string
     */
    protected function getChangeExtensionCommand($extension = 'txt')
    {
        return $this->createCommand(self::ACTION_CHANGE_CHARGE_FILE_EXTENSION, [
            'extension' => $extension,
        ]);
    }

    /**
     * Change extension for charge
     * @param string $extension
     */
    public function setExtension($extension = 'txt')
    {
        $cmd = $this->getChangeExtensionCommand($extension);

        static::exec($cmd);
    }
}
