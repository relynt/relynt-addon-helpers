<?php

namespace relynt\helpers;

use Yii;
use yii\base\BaseObject;
use yii\redis\Connection;

/**
 * Class RedisHelper
 * @package relynt\helpers
 */
class RedisHelper extends BaseObject
{
    /** Default prefix for all relynt keys stored in redis */
    const DEFAULT_RELYNT_REDIS_KEY_PREFIX = 'rel:';

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public static function set($key, $value)
    {
        return RedisHelper::getRedis()->set(RedisHelper::calculateKey($key), $value);
    }

    /**
     * @return Connection
     */
    public static function getRedis()
    {
        return Yii::$app->redis;
    }

    /**
     * @param string $key
     * @return string
     */
    public static function calculateKey($key)
    {
        return RedisHelper::DEFAULT_RELYNT_REDIS_KEY_PREFIX . $key;
    }

    /**
     * @param string $key
     * @param number $second
     * @param mixed $value
     */
    public static function setex($key, $second, $value)
    {
        RedisHelper::getRedis()->setex(RedisHelper::calculateKey($key), $second, $value);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        return RedisHelper::getRedis()->get(RedisHelper::calculateKey($key));
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function delete($key)
    {
        return (bool)RedisHelper::getRedis()->del(RedisHelper::calculateKey($key));
    }
}
