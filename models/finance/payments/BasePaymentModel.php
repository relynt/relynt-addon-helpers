<?php

namespace relynt\models\finance\payments;

use relynt\helpers\ConfigHelper;
use relynt\models\customer\BaseCustomer;
use relynt\models\finance\BankStatement;
use relynt\models\finance\BaseInvoice;
use relynt\models\finance\BasePayment;
use relynt\models\finance\BaseProformaInvoice;
use relynt\models\finance\BaseTransactions;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class BasePaymentModel
 * @package relynt\models\finance\payments
 * @property float $amount;
 * @property string $addonTitle;
 * @property BankStatement $bankStatement;
 * @property BasePayment $basePayment;
 */
abstract class BasePaymentModel extends Model
{
    const ADD_FEE = 'add_fee';
    const FEE_VAT = 'fee_vat';
    const FEE_MESSAGE = 'fee_message';
    const PAYMENT_METHOD = 'payment_method';
    const FEE_TRANSACTION_CATEGORY = 'fee_transaction_category';
    const ADD_FEE_TO_REQUEST = 'add_fee_to_request';

    /** @var integer */
    public $customer_id;

    /**
     * Default config names.
     *
     * @see [[getCustomConfigNames()]]
     * @see [[getConfigName($name)]]
     *
     * @var array associative array where key is config name, value is your default config name.
     */
    private $_defaultConfigNames = [
        self::ADD_FEE => 'serviceFee',
        self::FEE_VAT => 'fee_VAT',
        self::FEE_MESSAGE => 'fee_message',
        self::PAYMENT_METHOD => 'payment_method_id',
        self::FEE_TRANSACTION_CATEGORY => 'transaction_fee_category',
        self::ADD_FEE_TO_REQUEST => 'add_fee_request',
    ];

    /** @var float */
    private $_amount;

    /** @var BankStatement */
    private $_bank_statement;

    /** @var BasePayment */
    private $_basePayment;

    public function init()
    {
        $this->basePayment = new BasePayment();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'amount', 'addonTitle'], 'required'],
            [['customer_id'], 'integer'],
            [['amount'], 'number', 'min' => 0.01],
        ];
    }

    /**
     * Get Addon title for use in BankStatement.
     * @return string
     */
    abstract public function getAddonTitle();

    /**
     * Get BankStatement.
     *
     * @return BankStatement
     */
    public function getBankStatement()
    {
        return $this->_bank_statement;
    }

    /**
     * @param BasePayment $basePayment
     */
    public function setBasePayment(BasePayment $basePayment)
    {
        $this->_basePayment = $basePayment;
    }

    /**
     * @return BasePayment
     */
    public function getBasePayment()
    {
        return $this->_basePayment;
    }

    /**
     * Create BankStatement.
     *
     * @param array $params
     * @return $this|null|BankStatement
     */
    public function createBankStatement($params = [])
    {
        if ($this->validate(['customer_id', 'amount']) == false) {
            $message = implode(' ', array_map(function ($errors) {
                return implode(' ', $errors);
            }, $this->getErrors()));
            throw new InvalidParamException($message);
        }

        $attr = [
            'customer_id' => $this->customer_id,
            'amount' => $this->getTotalAmount(),
            'addonTitle' => $this->addonTitle,
            'payment_date' => date('Y-m-d')
        ];

        if ($this instanceof BasePaymentInvoice && $this->invoice instanceof BaseInvoice) {
            $attr['invoice_id'] = $this->invoice->id;
        } elseif ($this instanceof BasePaymentProformaInvoice && $this->invoice instanceof BaseProformaInvoice) {
            $attr['request_id'] = $this->invoice->id;
        }

        if (is_array($params)) {
            $attr = ArrayHelper::merge($attr, $params);
        }
        $bankStatement = new BankStatement($attr);
        $this->_bank_statement = $bankStatement->createBankStatement();
        if ($this->_bank_statement == null) {
            $this->addError('bankStatement', Yii::t('app', 'Error adding bank statement!'));
            $this->addErrors($bankStatement->errors);
        }
        return $this->_bank_statement;
    }

    /**
     * Find and set BankStatement By id or BankStatement model
     * @param BankStatement|integer $bankStatement
     */
    public function setBankStatement($bankStatement)
    {
        if ($bankStatement instanceof BankStatement) {
            $this->_bank_statement = $bankStatement;
        } elseif (is_integer($bankStatement)) {
            $this->_bank_statement = (new BankStatement())->findById($bankStatement);
        }
    }

    /**
     * @param string $status
     * @return bool
     */
    public function setBankStatementStatus($status)
    {
        if (!$this->_bank_statement) {
            return false;
        }
        $this->_bank_statement->status = $status;
        return $this->_bank_statement->save();
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        if ($this instanceof BasePaymentInvoice || $this instanceof BasePaymentProformaInvoice) {
            return round($this->invoice->total, 2);
        }
        return $this->_amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->_amount = $amount;
    }

    /**
     * @return null|mixed
     */
    public function getFee()
    {
        return ConfigHelper::get($this->getConfigName(self::ADD_FEE));
    }

    /**
     * @return bool
     */
    public function isFee()
    {
        return is_numeric($this->getFee()) and $this->getFee() != 0;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return round($this->amount + $this->getFeeAmount(), 2);
    }

    /**
     * @param float|int|null $amount
     * @return float|int
     */
    public function getFeeAmount($amount = null)
    {
        if ($this->isFee()) {
            return round(($amount !== null ? $amount : $this->amount) / 100 * $this->getFee(), 2);
        } else {
            return 0;
        }
    }

    /**
     * @return null|float
     */
    public function getFeeVat()
    {
        return ConfigHelper::get($this->getConfigName(self::FEE_VAT));
    }

    /**
     * @return bool
     */
    public function isFeeVAT()
    {
        return is_numeric($this->getFeeVat()) and $this->getFeeVat() != 0;
    }

    /**
     * @return float|int
     */
    public function getFeeWithoutVAT()
    {
        return $this->getFeeAmount() / (100 + $this->getFeeVat()) * 100;
    }

    /**
     * @param float|null $amount
     * @return float|int
     */
    public function getAmountWithoutFee($amount = null)
    {
        return ($amount !== null ? $amount : $this->amount) / (100 + $this->getFee()) * 100;
    }

    /**
     * Rename config options.
     *
     * You can change default names of config options.
     * Available names to change, see [[_defaultConfigNames]].
     *
     * ```php
     *
     * return [
     *      BasePaymentModel::ADD_FEE => 'yourNewConfigNameForFee',
     *      BasePaymentModel::FEE_VAT => 'yourNewConfigNameForFeeVat',
     * ];
     *
     * ```
     *
     * @return array associative array where key is config name which you want to redefine, value is your config name.
     */
    public function getCustomConfigNames()
    {
        return [];
    }

    /**
     * Get config name.
     * If config option name is not defined in [[getCustomConfigNames()]] method, default value will be used. See [[defaultConfigNames]].
     *
     * @param string $name config name.
     * @return mixed
     *
     * @throws \InvalidArgumentException if name is invalid.
     */
    public function getConfigName($name)
    {
        if (isset($this->getCustomConfigNames()[$name])) {
            return $this->getCustomConfigNames()[$name];
        } elseif (isset($this->_defaultConfigNames[$name])) {
            return $this->_defaultConfigNames[$name];
        } else {
            throw new \InvalidArgumentException(Yii::t('app', 'Invalid config name: {name}', ['name' => $name]));
        }
    }

    /**
     *Set Amount without Fee
     * @param float|null $amount
     */
    public function setAmountWithOutFee($amount = null)
    {
        if ($this->isFee()) {
            $amountWithFee = $amount ? $amount : $this->amount;
            // Set amounts
            $this->amount = $amountWithFee / (100 + $this->getFee()) * 100;
        } else {
            $this->amount = $amount;
        }
    }

    /**
     * @param null|string $apiPaymentId
     * @param integer|null $bankStatementId
     * @return bool
     */
    public function processPayment($apiPaymentId = null, $bankStatementId = null)
    {
        if ($this->validate() == false) {
            return false;
        }
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->getBankStatementById($bankStatementId);
            if (!$bankStatement || !in_array($bankStatement->status, [BankStatement::STATUS_NEW, BankStatement::STATUS_PENDING], true)) {
                return false;
            }
            $this->bankStatement = $bankStatement;
        } else {
            $bankStatement = $this->bankStatement;
        }

//        $this->setAmountWithOutFee();

        if ($this->isFee()) {
            // Invoices and proforma
            if ($this instanceof BasePaymentInvoice or ($this instanceof BasePaymentProformaInvoice and ConfigHelper::get($this->getConfigName(self::ADD_FEE_TO_REQUEST)) == true)) {
                //Add item with fee to invoice
                $fee_message = ConfigHelper::get($this->getConfigName(self::FEE_MESSAGE));
                $fee_item = [
                    'price' => ($this->isFeeVAT()) ? $this->getFeeWithoutVAT() : $this->getFeeAmount(),
                    'description' => $fee_message ? $fee_message : 'Commission',
                    'quantity' => 1,
                    'tax' => ($this->isFeeVAT()) ? $this->getFeeVat() : 0,
                ];
                $this->invoice->items[] = $fee_item;
                $this->invoice->update();
            } else {
                // Top-ups
                // Increase amount with fee
                $this->amount += $this->getFeeAmount();
            }
        }
        $createdPaymentId = $this->createPayment($apiPaymentId);
        if ($createdPaymentId === false) {
            return false;
        }
        if ($this->isFee() and !($this instanceof BasePaymentInvoice)) {
            $this->createTransaction($createdPaymentId);
        }

        $bankStatement->status = BankStatement::STATUS_PROCESSED;
        $bankStatement->payment_id = $createdPaymentId;

        // Update bank statement
        $bankStatement->update();
        return true;
    }

    /**
     * @param null $apiPaymentId
     * @return bool|integer
     */
    public function createPayment($apiPaymentId = null)
    {
        // Create Relynt payment
        $payment = $this->_basePayment;
        $payment->customer_id = $this->customer_id;
        $payment->amount = $this->bankStatement->amount; //amount with fee (total amount)
        $partnerId = BaseCustomer::findIdentity($this->customer_id)->partner_id;
        $payment->payment_type = ConfigHelper::get($this->getConfigName(self::PAYMENT_METHOD), $partnerId);
        if ($this instanceof BasePaymentInvoice) {
            $payment->invoice_id = $this->invoice->id;
        } elseif ($this instanceof BasePaymentProformaInvoice) {
            $payment->request_id = $this->invoice->id;
        }
        $payment->comment = 'Pay by ' . $this->addonTitle;
        if ($apiPaymentId) {
            $payment->field_4 = 'Payment: ' . $apiPaymentId;
        }
        if ($this instanceof BasePaymentInvoice) {
            $payment->field_5 = 'Invoice: ' . $this->invoice->id;
        } elseif ($this instanceof BaseProformaInvoice) {
            $payment->field_5 = 'Request: ' . $this->invoice->id;
        } else {
            $payment->field_5 = 'Bank Statement: ' . $this->bankStatement->id;
        }

        if ($payment->create()) {
            return $payment->id;
        }
        if (count($payment->errors) > 0) {
            $this->addErrors($payment->errors);
        } else {
            $this->addError('payment', Yii::t('app', 'Payment create failed'));
        }
        return false;
    }

    /**
     * @param integer $createdPaymentId
     * @return bool|integer
     */
    public function createTransaction($createdPaymentId)
    {
        $feeAmount = $this->getFeeAmount($this->getAmountWithoutFee($this->bankStatement->amount)); //get fee amount from total amount

        $transaction = new BaseTransactions();

        if ($this->isFeeVAT()) {
            $feeAmount = $feeAmount / (100 + $this->getFeeVat()) * 100;
            $transaction->tax_percent = $this->getFeeVat();
        }

        $transaction->customer_id = $this->customer_id;
        $transaction->type = BaseTransactions::TYPE_DEBIT;
        $transaction->price = $feeAmount;
        $transaction->category = ConfigHelper::get($this->getConfigName(self::FEE_TRANSACTION_CATEGORY));
        $transaction->description = $this->addonTitle . ' fee (for payment #' . $createdPaymentId . ')';
        $transaction->source = BaseTransactions::SOURCE_MANUAL;
        $transaction->create();

        if ($transaction->create()) {
            return $transaction->id;
        }
        if (count($transaction->errors) > 0) {
            $this->addErrors($transaction->errors);
        } else {
            $this->addError('transaction', Yii::t('app', 'Transaction create failed'));
        }
        return false;
    }

    /**
     * @param null|int $bankStatementId
     */
    public function cancel($bankStatementId = null)
    {
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->findById($bankStatementId);
        } else {
            $bankStatement = $this->bankStatement;
        }
        if ($bankStatement !== null && in_array($bankStatement->status, [BankStatement::STATUS_PENDING, BankStatement::STATUS_NEW], true)) {
            $bankStatement->status = BankStatement::STATUS_CANCELED;
            $bankStatement->update();
        }
    }

    /**
     * @param null $bankStatementId
     */
    public function error($bankStatementId = null)
    {
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->findById($bankStatementId);
        } else {
            $bankStatement = $this->bankStatement;
        }
        if ($bankStatement !== null && in_array($bankStatement->status, [BankStatement::STATUS_PENDING, BankStatement::STATUS_NEW], true)) {
            $bankStatement->status = BankStatement::STATUS_ERROR;
            $bankStatement->update();
        }
    }
}
