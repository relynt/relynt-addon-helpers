<?php

namespace relynt\models\finance;

use relynt\base\BaseActiveApi;

/**
 * Class BaseTransactions
 * @package relynt\models\finance
 */
class BaseTransactions extends BaseActiveApi
{

    public $id;
    public $customer_id;
    public $type;
    public $quantity = 1;
    public $unit;
    public $price;
    public $tax_percent;
    public $total;
    public $date;
    public $category;
    public $description;
    public $period_from;
    public $period_to;
    public $service_id;
    public $payment_id;
    public $invoice_id;
    public $invoiced_by_id;
    public $comment;
    public $to_invoice;
    public $service_type;
    public $source;
    public $additional_attributes = [];

    const SOURCE_MANUAL = 'manual';
    const SOURCE_AUTO = 'auto';
    const SOURCE_CDR = 'cdr';

    const TYPE_DEBIT = 'debit';
    const TYPE_CREDIT = 'credit';

    const SERVICE_TYPE_INTERNET = 'internet';
    const SERVICE_TYPE_VOICE = 'voice';
    const SERVICE_TYPE_CUSTOM = 'custom';

    public static $apiUrl = 'admin/finance/transactions';
}
