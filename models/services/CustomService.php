<?php

namespace relynt\models\services;

use relynt\base\BaseApiModel;
use relynt\helpers\ApiHelper;
use yii\helpers\Html;

/**
 * Class CustomService
 * @package relynt\models\services
 * @deprecated
 */
class CustomService extends BaseApiModel
{
    public $id;
    public $type;
    public $parent_id;
    public $customer_id;
    public $tariff_id;
    public $description;
    public $quantity;
    public $unit;
    public $unit_price;
    public $start_date;
    public $end_date;
    public $discount;
    public $discount_percent;
    public $discount_start_date;
    public $discount_end_date;
    public $discount_text;
    public $status;
    public $status_new;
    public $additional_attributes = [];

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_STOPPED = 'stopped';
    const STATUS_PENDING = 'pending';

    public static $baseApiCall = 'admin/customers/customer';

    public static $serviceApiCall = 'custom-services';

    public static function getApiCall($customer_id, $service_id = null)
    {
        $uri = self::$baseApiCall . '/' . $customer_id . '/' . self::$serviceApiCall;

        if ($service_id !== null) {
            $uri .= '--' . $service_id;
        }

        return $uri;
    }

    public static $services;

    /**
     * @param $customer_id
     * @return CustomService[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function findByCustomerId($customer_id)
    {
        if (self::$services === null or !isset(self::$services[$customer_id])) {
            $result = ApiHelper::getInstance()->get(self::getApiCall($customer_id));

            if ($result['result'] == false or empty($result['response'])) {
                self::$services[$customer_id] = [];
            } else {
                foreach ($result['response'] as $serviceData) {
                    $service = new self;
                    static::populate($service, $serviceData);
                    self::$services[$customer_id][$service->id] = $service;
                }
            }
        }

        return self::$services[$customer_id];
    }

    public static function findOneByCustomerIdAndId($customer_id, $service_id)
    {
        $services = self::findByCustomerId($customer_id);
        if (empty($services)) {
            return [];
        }

        foreach ($services as $serviceId => $service) {
            if ($serviceId == $service_id) {
                return $service;
            }
        }

        return [];
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                $class = 'primary';
                break;
            case self::STATUS_PENDING:
                $class = 'warning';
                break;
            case self::STATUS_STOPPED:
                $class = 'danger';
                break;
            case self::STATUS_DISABLED:
            default:
                $class = 'default';
                break;
        }
        return Html::tag('span', $this->status, [
            'class' => 'label label-' . $class
        ]);
    }
}
