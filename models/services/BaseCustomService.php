<?php

namespace relynt\models\services;

/**
 * Class BaseCustomService
 * @package relynt\models\services
 */
class BaseCustomService extends BaseService
{
    public function getServiceApiUrl()
    {
        return 'custom-services';
    }
}
