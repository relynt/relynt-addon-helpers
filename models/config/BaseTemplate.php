<?php
namespace relynt\models\config;

use relynt\base\BaseActiveApi;
use relynt\helpers\ApiHelper;
use yii\base\InvalidParamException;

class BaseTemplate extends BaseActiveApi
{
    public $id;
    public $type;
    public $title;
    public $description;

    protected static $apiUrl = 'admin/config/templates';

    const TYPE_CUSTOMER_PORTAL = 'customer-portal';
    const TYPE_INVOICE_PDF = 'invoice-pdf';
    const TYPE_MAIL = 'mail';
    const TYPE_SMS = 'sms';
    const TYPE_DOCUMENTS = 'documents';
    const TYPE_CARDS = 'cards';
    const TYPE_PAYMENT_CALENDAR = 'payment-calendar';
    const TYPE_PAYMENT_RECEIPT = 'payment-receipt';
    const TYPE_REQUEST_PDF = 'request-pdf';
    const TYPE_REMINDER_MAIL = 'reminder-mail';
    const TYPE_REMINDER_SMS = 'reminder-sms';
    const TYPE_MONITORING_NOTIFICATION_MAIL = 'monitoring-notification-mail';
    const TYPE_MONITORING_NOTIFICATION_SMS = 'monitoring-notification-sms';
    const TYPE_EXPORTS = 'exports';
    const TYPE_TICKETS = 'tickets';
    const TYPE_MAIL_CAP = 'mail_cap';
    const TYPE_SMS_CAP = 'sms_cap';
    const TYPE_INTERNAL = 'internal';
    const TYPE_MAIL_FUP = 'mail_fup';
    const TYPE_SMS_FUP = 'sms_fup';
    const TYPE_REPORT_STATEMENTS = 'report_statements';

    public function rules()
    {
        return [
            [['type', 'title'], 'required'],
            [['title'], 'string', 'min' => 3, 'max' => 128],
            [['description'], 'string', 'max' => 512],
            [['type'], 'in', 'range' => self::getTemplateTypes()]
        ];
    }

    /**
     * get template array
     * @return array
     */
    public function getTemplateTypes()
    {
        return [
            self::TYPE_CUSTOMER_PORTAL,
            self::TYPE_INVOICE_PDF,
            self::TYPE_MAIL,
            self::TYPE_SMS,
            self::TYPE_DOCUMENTS,
            self::TYPE_CARDS,
            self::TYPE_PAYMENT_CALENDAR,
            self::TYPE_PAYMENT_RECEIPT,
            self::TYPE_REQUEST_PDF,
            self::TYPE_REMINDER_MAIL,
            self::TYPE_REMINDER_SMS,
            self::TYPE_MONITORING_NOTIFICATION_MAIL,
            self::TYPE_MONITORING_NOTIFICATION_SMS,
            self::TYPE_EXPORTS,
            self::TYPE_TICKETS,
            self::TYPE_MAIL_CAP,
            self::TYPE_SMS_CAP,
            self::TYPE_INTERNAL,
            self::TYPE_MAIL_FUP,
            self::TYPE_SMS_FUP,
            self::TYPE_REPORT_STATEMENTS,
        ];
    }

    /**
     * Render Template including Customer data.
     * @param $customer_id
     * @param $template_id
     * @return null|string
     * @throws \yii\base\InvalidParamException
     */
    public function renderTemplate($customer_id, $template_id)
    {
        if (!$customer_id || !$template_id) {
            throw new InvalidParamException("customer_id an render_id must be set");
        }
        $result = ApiHelper::getInstance()->get(static::$apiUrl, "{$template_id}-render-{$customer_id}");
        if ($result['result'] && !empty($result['response'])) {
            return isset($result['response']['result']) ? $result['response']['result'] : null;
        }
        return null;
    }
}
