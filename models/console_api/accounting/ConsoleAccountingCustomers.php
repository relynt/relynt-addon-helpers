<?php

namespace relynt\models\console_api\accounting;

use relynt\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package relynt\models\console_api\accounting
 */
class ConsoleAccountingCustomers extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-customers';

    /** @var int */
    public $customer_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['customer_id', 'integer']
        ], parent::rules());
    }
}
