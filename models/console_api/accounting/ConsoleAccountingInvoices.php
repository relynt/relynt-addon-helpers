<?php

namespace relynt\models\console_api\accounting;

use relynt\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package relynt\models\console_api\accounting
 */
class ConsoleAccountingInvoices extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-invoices';

    /** @var int */
    public $invoice_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['invoice_id', 'integer']
        ], parent::rules());
    }
}
