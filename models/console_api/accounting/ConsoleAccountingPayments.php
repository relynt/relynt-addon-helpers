<?php

namespace relynt\models\console_api\accounting;

use relynt\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package relynt\models\console_api\accounting
 */
class ConsoleAccountingPayments extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-payments';

    /** @var int */
    public $payment_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['payment_id', 'integer']
        ], parent::rules());
    }
}
