<?php

namespace relynt\models\console_api\einvoicing;

use relynt\helpers\ArrayHelper;

/**
 * Class ConsoleEInvoicingInvoices
 * @package relynt\models\console_api\einvoicing
 */
class ConsoleEInvoicingInvoices extends ConsoleBaseEInvoicing
{
    public static $controllerName = 'e-invoicing-invoices';

    /** @var int */
    public $invoice_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['invoice_id', 'integer']
        ], parent::rules());
    }
}
