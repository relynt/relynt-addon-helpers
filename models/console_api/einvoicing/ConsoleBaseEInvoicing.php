<?php

namespace relynt\models\console_api\einvoicing;

use relynt\base\BaseConsoleModel;

/**
 * Class ConsoleBaseEInvoicing
 * @package relynt\models\console_api\einvoicing
 *
 * @property-read array|int[] $eInvoicingStatuses
 */
abstract class ConsoleBaseEInvoicing extends BaseConsoleModel
{
    public $id;
    public $customer_id;
    public $modified;
    public $einvoicing_id;
    public $einvoicing_status;
    public $create_date;
    public $updated_date;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;

    //einvoicing statuses
    const EINVOICING_STATUS_NEW = 0;
    const EINVOICING_STATUS_PENDING = 1;
    const EINVOICING_STATUS_UNKNOWN = 2;
    const EINVOICING_STATUS_ERROR = 3;
    const EINVOICING_STATUS_OK = 4;

    const EINVOICING_NOT_MODIFIED = '0';
    const EINVOICING_MODIFIED = '1';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['modified', 'in', 'range' => [static::EINVOICING_NOT_MODIFIED, static::EINVOICING_MODIFIED]],
            ['einvoicing_id', 'string'],
            ['einvoicing_status', 'in', 'range' => $this->getEInvoicingStatuses()],
            [['create_date', 'updated_date'], 'datetime', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            [['field_1', 'field_2', 'field_3', 'field_4', 'field_5'], 'string', 'max' => 256],
        ];
    }

    /**
     * @return array
     */
    public function getEInvoicingStatuses()
    {
        return [
            static::EINVOICING_STATUS_NEW,
            static::EINVOICING_STATUS_PENDING,
            static::EINVOICING_STATUS_UNKNOWN,
            static::EINVOICING_STATUS_ERROR,
            static::EINVOICING_STATUS_OK,
        ];
    }

    /**
     * @param string|null $startDate
     * @return mixed
     */
    public function sync($startDate = null)
    {
        return $this->customAction('sync', ['startDate' => $startDate]);
    }
}
