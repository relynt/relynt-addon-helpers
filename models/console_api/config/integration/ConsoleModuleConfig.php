<?php

namespace relynt\models\console_api\config\integration;

use relynt\base\BaseConsoleModel;
use relynt\base\BaseInstallController;
use relynt\helpers\ConfigHelper;

/**
 * Class ConsoleModuleConfig
 * @package relynt\models\console_api\config\integration
 */
class ConsoleModuleConfig extends BaseConsoleModel
{
    public static $controllerName = 'module-config';
    protected static $primaryKeys = ['module'];

    public $module;
    public $title;
    public $root;
    public $type;
    public $icon;
    public $path;
    public $status;

    public $title_attribute_prefix;
    public $title_attribute_suffix;

    const TYPE_SIMPLE = 'simple';
    const TYPE_EXTENDED = 'extended';
    const TYPE_ADDON = 'add-on';

    const DEFAULT_ICON = 'fa-cog';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'title', 'root', 'icon', 'type', 'title_attribute_prefix', 'title_attribute_suffix'], 'string'],
            [['module', 'title', 'type'], 'required'],
            ['type', 'in', 'allowArray' => true, 'range' => $this->getTypes()],
            ['root', 'required', 'when' => function ($model) {
                return $model->type !== static::TYPE_ADDON;
            }],
            ['status', 'in', 'range' => [BaseInstallController::MODULE_STATUS_ENABLED, BaseInstallController::MODULE_STATUS_DISABLED]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        if ($this->type === null) {
            $this->type = static::TYPE_ADDON;
        }
        //moduleConfig status is only for relynt version 2.3
        if (version_compare(ConfigHelper::getSpynxVersion(), '2.3') === -1) {
            $this->status = null;
            return;
        }
        if ($this->type != static::TYPE_ADDON && $this->status === null) {
            $this->status = BaseInstallController::MODULE_STATUS_ENABLED;
        }
    }

    /**
     * @return array
     */
    private function getTypes()
    {
        return [static::TYPE_SIMPLE, static::TYPE_ADDON, static::TYPE_EXTENDED];
    }
}
