<?php

namespace relynt\models\console_api\config;

use relynt\base\BaseConsoleModel;

/**
 * Class ConsoleAdditionalField
 * You could find additional fields only by module (findAll(['module' => 'customers'])) or
 * module and name (findOne(['module' => 'customers', 'name' => 'test'])). Other parameters will be ignored
 * @package relynt\models\console_api\config
 */
class ConsoleAdditionalField extends BaseConsoleModel
{
    public static $controllerName = 'additional-fields';
    protected static $primaryKeys = ['module', 'name'];

    // types
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_NUMERIC = 'numeric';
    const TYPE_DATE = 'date';
    const TYPE_DATETIME = 'datetime';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_SELECT = 'select';
    const TYPE_SELECT_MULTIPLE = 'select_multiple';
    const TYPE_RELATION = 'relation';
    const TYPE_RELATION_MULTIPLE = 'relation_multiple';
    const TYPE_PASSWORD = 'password';
    const TYPE_FILE = 'file';
    const TYPE_ADDON = 'add-on';
    const TYPE_IP = 'ip';
    const TYPE_TEXTAREA = 'textarea';

    // addon input types
    const ADDON_INPUT_TYPE_BUTTON = 'button';
    const ADDON_INPUT_TYPE_TEXT = 'text';
    const ADDON_INPUT_TYPE_TEXT_PLUS_BUTTON = 'text_plus_button';

    public $module;
    public $name;
    public $title;
    public $type;
    public $position;
    public $default_value;
    public $min_length;
    public $max_length;
    public $select_values;
    public $decimals;
    public $is_required;
    public $is_unique;
    public $is_add;
    public $show_in_list;
    public $searchable;
    public $readonly;
    public $disabled;
    public $relation_module;
    public $addon;
    public $addon_uri;
    public $addon_input_type;
    public $set_default_value = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'module',
                    'title',
                    'type',
                    'default_value',
                    'select_values',
                    'relation_module',
                    'addon',
                    'addon_uri',
                    'addon_input_type',
                ],
                'string'],
            [
                [
                    'is_required',
                    'is_unique',
                    'is_add',
                    'show_in_list',
                    'searchable',
                    'readonly',
                    'set_default_value',
                    'disabled'],
                'boolean'],

            [['name', 'module', 'title', 'type'], 'required'],
            ['decimals', 'integer'],
            [['min_length', 'max_length'], $this->type == static::TYPE_DECIMAL ? 'double' : 'integer', 'min' => 0],
            ['select_values', 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_SELECT or $model->type == static::TYPE_SELECT_MULTIPLE);
            }],
            ['decimals', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_DECIMAL;
            }],
            ['relation_module', 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_RELATION or $model->type == static::TYPE_RELATION_MULTIPLE);
            }],
            ['addon', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_ADDON;
            }],
            ['addon_uri', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_ADDON;
            }],
            ['addon_input_type', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_ADDON;
            }],
            ['addon_input_type', 'in', 'allowArray' => true, 'range' => $this->getAddonInputTypes()],
            ['type', 'in', 'allowArray' => true, 'range' => $this->getFieldsTypes()],
        ];
    }

    /**
     * @return array
     */
    private function getAddonInputTypes()
    {
        return [static::ADDON_INPUT_TYPE_BUTTON, static::ADDON_INPUT_TYPE_TEXT, static::ADDON_INPUT_TYPE_TEXT_PLUS_BUTTON];
    }

    /**
     * @return array
     */
    private function getFieldsTypes()
    {
        return [
            static::TYPE_STRING,
            static::TYPE_INTEGER,
            static::TYPE_DECIMAL,
            static::TYPE_NUMERIC,
            static::TYPE_DATE,
            static::TYPE_DATETIME,
            static::TYPE_BOOLEAN,
            static::TYPE_SELECT,
            static::TYPE_SELECT_MULTIPLE,
            static::TYPE_RELATION,
            static::TYPE_RELATION_MULTIPLE,
            static::TYPE_PASSWORD,
            static::TYPE_FILE,
            static::TYPE_ADDON,
            static::TYPE_IP,
            static::TYPE_TEXTAREA,
        ];
    }
}
