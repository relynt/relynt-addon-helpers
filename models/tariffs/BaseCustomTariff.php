<?php

namespace relynt\models\tariffs;

/**
 * Class BaseCustomTariff
 * @package relynt\models\tariffs
 */
class BaseCustomTariff extends BaseTariff
{
    public static $apiUrl = 'admin/tariffs/custom';
}
