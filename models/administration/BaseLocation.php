<?php

namespace relynt\models\administration;

use relynt\base\BaseActiveApi;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class BaseLocation
 * @package relynt\models\administration
 */
class BaseLocation extends BaseActiveApi
{
    public $id;
    public $name;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/locations';


    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
