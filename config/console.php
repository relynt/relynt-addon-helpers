<?php

use relynt\helpers\ConfigHelper;
use yii\helpers\ArrayHelper;

/**
 * Base configuration for Relynt Add-Ons console applications.
 * In your add-on's console.php you can use special classes for rewrite or unset settings.
 * For rewrite settings use yii\helpers\ReplaceArrayValue class instead of setting value.
 * For uset settings use yii\helpers\UnsetArrayValue class instead of setting value.
 * @param array $params Settings from config/params.php file
 * @param string $baseDir Path to add-on directory
 * @return array
 */
return function ($params, $baseDir) {
    return ArrayHelper::merge(require 'common.php', [
        // Generate application id by add-on directory name
        'id' => ConfigHelper::getAddOnNameFromBaseDir($baseDir) . '-console',
        'basePath' => $baseDir,
        'vendorPath' => "{$baseDir}/../relynt-addon-base-2/vendor",
        // Path to console controllers
        'controllerNamespace' => 'app\commands',
        'components' => [
            'urlManager' => [
                'scriptUrl' => '',
            ],
        ],
        'params' => $params,
    ]);
};
