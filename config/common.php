<?php

use relynt\helpers\ConfigHelper;

return [
    'bootstrap' => ['log'],
    // set Relynt timeZone
    'timeZone' => ConfigHelper::getRelyntTimeZone(),
    'api' => [
        'version' => RelyntApi::API_VERSION_2
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:@app/data/data.db',
            'charset' => 'utf8'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => ConfigHelper::getSessionConfig(),
        'redis' => ConfigHelper::getRedisConfigComponent(),
        'formatter' => [
            'timeZone' => ConfigHelper::getRelyntTimeZone(),
            'defaultTimeZone' => ConfigHelper::getRelyntTimeZone(),
            'dateFormat' => 'php:Y-m-d',
            'timeFormat' => 'php:H:i:s',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
        ],
        'log' => [
            'class' => 'relynt\components\log\RelyntDispatcher',
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'levels' => ['info'],
                    'logFile' => '@app/runtime/logs/info.log',
                    'class' => 'yii\log\FileTarget',
                    'maxFileSize' => 1024 * 10,
                    // Do not add $_SESSION, $_SERVER... to logs
                    'logVars' => [],
                    // Remove IP and other information from message prefix
                    'prefix' => function ($message) {
                        return '';
                    }
                ],
                [
                    'levels' => ['warning'],
                    'logFile' => '@app/runtime/logs/warning.log',
                    'class' => 'yii\log\FileTarget',
                    'maxFileSize' => 1024 * 10,
                    'logVars' => [],
                    'prefix' => function ($message) {
                        return '';
                    }
                ],
                [
                    'levels' => ['error'],
                    'logFile' => '@app/runtime/logs/error.log',
                    'class' => 'yii\log\FileTarget',
                    'maxFileSize' => 1024 * 10,
                    'prefix' => function ($message) {
                        return '';
                    }
                ],
            ],
        ],
    ]
];
