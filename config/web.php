<?php

use relynt\helpers\ConfigHelper;
use yii\helpers\ArrayHelper;
use relynt\v2\models\customer\BaseCustomer;

/**
 * Base configuration for Relynt Add-Ons web applications.
 * In your add-on's web.php you can use special classes for rewrite or unset settings.
 * For rewrite settings use yii\helpers\ReplaceArrayValue class instead of setting value.
 * For uset settings use yii\helpers\UnsetArrayValue class instead of setting value.
 * @param array $params Settings from config/params.php file
 * @param string $baseDir Path to add-on directory
 * @return array
 */
return function ($params, $baseDir) {
    $addonBasePath = "{$baseDir}/../relynt-addon-base-2";

    $config = [
        // Generate application id by add-on directory name
        'id' => ConfigHelper::getAddOnNameFromBaseDir($baseDir) . '-web',
        'basePath' => $baseDir,
        // Path to relynt-addon-base-2 vendor directory
        'vendorPath' => "$addonBasePath/vendor",
        // Settings from config/params.php
        'params' => $params,
        'aliases' => [
            '@bower' => "$addonBasePath/vendor/bower-asset",
            '@npm'   => "$addonBasePath/vendor/npm-asset",
        ],
        'components' => [
            'request' => [
                'cookieValidationKey' => $params['cookieValidationKey'],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'user' => [
                'identityClass' => BaseCustomer::class,
                'idParam' => 'relynt_customer_id',
                'loginUrl' => '/portal/',
                'enableAutoLogin' => true,
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                // Load url rules from config/url_rules.php file
                'rules' => ConfigHelper::getUrlRules($baseDir),
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@app/messages',
                        'sourceLanguage' => 'en-US',
                    ]
                ]
            ],
            'view' => [
                'class' => 'yii\web\View',
                'defaultExtension' => 'twig',
                'renderers' => [
                    'twig' => [
                        'class' => 'yii\twig\ViewRenderer',
                        'cachePath' => '@runtime/Twig/cache',
                        // Array of twig options:
                        'options' => [
                            'cache' => false
                        ],
                        'extensions' => [
                            '\relynt\components\twig\TwigActiveForm',
                            '\relynt\components\twig\TwigPjax'
                        ],
                        'globals' => [
                            'Html' => ['class' => '\yii\helpers\Html'],
                            'Url' => ['class' => '\yii\helpers\Url'],
                            'GridView' => ['class' => '\yii\grid\GridView'],
                            'DatePicker' => ['class' => '\yii\jui\DatePicker'],
                            'DateRangePicker' => ['class' => '\relynt\widgets\DateRangePicker'],
                            'MaskedInput' => ['class' => '\yii\widgets\MaskedInput'],
                            'Yii' => ['class' => 'Yii'],
                            'DateHelper' => ['class' => '\relynt\helpers\DateHelper'],
                            'Select2' => ['class' => '\relynt\widgets\Select2'],
                        ],
                        'uses' => [
                            'yii\bootstrap'
                        ]
                    ],
                ],
            ],
            'assetManager' => [
                'forceCopy' => YII_ENV_DEV,
            ],
        ],
    ];

    if (YII_ENV_DEV) {
        // Configuration adjustments for 'dev' environment
        $config['components']['view']['renderers']['twig']['options']['debug'] = true;
        $config['components']['view']['renderers']['twig']['options']['auto_reload'] = true;
        $config['components']['view']['renderers']['twig']['extensions'][] = '\Twig\Extension\DebugExtension';

        // Enable yii-debug module
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*'],
        ];
    }

    return ArrayHelper::merge(require 'common.php', $config);
};
